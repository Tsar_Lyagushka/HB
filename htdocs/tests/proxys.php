<?php
//Définition de la constante anti-hacking
define("INDEX", 1);
define("DEBUG", true);

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('../.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu

function traiterfichier($uri, &$list)
{
    $fp = fopen($uri, "r");
    while (!feof($fp)) {
        $buffer = fgets($fp);
        if (preg_match("#^([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3})#", $buffer, $match)) {
            var_dump($match);
        }
    }
    fclose($fp);
}

$list = array();
traiterfichier("http://www.proxylists.net/socks4.txt", $list);
traiterfichier("http://www.proxylists.net/socks5.txt", $list);
traiterfichier("http://www.proxylists.net/http_highanon.txt", $list);
traiterfichier("http://www.proxylists.net/http.txt", $list);

$bdd = new BDD();
$bdd->query("TRUNCATE TABLE  `proxy_list`;");
$bdd->query("INSERT INTO proxy_list VALUES ('".implode("'),('", $list)."');");
$bdd->deconnexion();

print "Procédure terminée, ".count($list)." proxys listés.";
