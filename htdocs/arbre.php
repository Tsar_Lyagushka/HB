<?php
define("INDEX", 1); //Définition de la constante anti-hacking
//Lancement du chronomètre de temps de la page
$chrono_start = microtime();
//Inclusion de l'API Onyx
$onyx = @file_get_contents('./.onyx') or die("Configuration introuvable.");
define("_FCORE", str_replace('/onyx2/load.php', '/onyx/', trim($onyx)));
require_once(_FCORE."common.php");
$race = 'humain';
require_once(_FCORE."../game/noms.php");
require_once(_FCORE."../game/vars.php");

$tree = array();

foreach ($technologiesCALC as $keyB => $branche) {
    print "<h3><ins>Branche :</ins> ".$technologiesVAR[$keyB]."</h3>";
    foreach ($branche as $keyT => $tech) {
        print $tech[0].' : '.$technolo[$keyB][$keyT]."<br />";
        if ($tech[1] > 0) {
            print '<em><ins>Requiert :</ins>';
            foreach ($branche as $keyR => $req) {
                if ($req[0]&$tech[1]) {
                    print ' '.$technolo[$keyB][$keyR].',';
                }
            }
            print '</em><br />';
        } else {
            print '<em>Ne requiert aucune technologie</em><br />';
            $tree[] = array($tech);
        }
        print "<br />";
    }
}

echo("\n<!--Page générée en : ".round((array_sum(explode(' ', microtime()))) - (array_sum(explode(' ', $chrono_start))), 4)." secondes.-->");
