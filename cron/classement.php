<?php

// Ce cron met à jour le classement des joueurs.

//Définition de la constante anti-hacking
define("INDEX", 1);
define("DEBUG", false);

//Inclusion de l'API Onyx
require_once(__DIR__ . "/../onyx2/load.php");
require_once("common.php");

$bdd = new BDD();

$updated = $bdd->update_classement();
$bdd->deconnexion();
