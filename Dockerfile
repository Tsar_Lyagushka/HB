FROM php:7.2-fpm-alpine

RUN apk add --no-cache libpng libpng-dev freetype freetype-dev && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ && \
    docker-php-ext-install gd mysqli

COPY . /usr/src/hb-main/

WORKDIR /usr/src/hb-main
ENTRYPOINT ["/usr/src/hb-main/entrypoint.sh"]
CMD ["php-fpm"]

RUN chmod 777 /usr/src/hb-main/onyx2/cache /usr/src/hb-main/onyx2/cache/signatures /usr/src/hb-main/onyx2/log /usr/src/hb-main/onyx2/log/users /usr/src/hb-main/onyx2/modules/templates/cache /usr/src/hb-main/onyx2/modules/templates/compile