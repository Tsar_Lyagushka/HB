<?php
if (!defined('ONYX')) {
    exit;
}

$hash = gpc('signer');

$page = 'alliance/nm_signer';
if (!empty($planete->id_alliance)) {
    erreur("Vous êtes déjà dans une alliance, vous ne pouvez pas ratifier d'alliance !", "red");
} elseif (!empty($_POST["sign"])) {
    if (trim(strtolower(gpc("sign", "post"))) == "oui") {
        $hash = intval($hash);
        $bdd->reconnexion();
        $ratifier = $bdd->unique_query("SELECT id, fondateur, signatures FROM $table_alliances_creation WHERE signatures LIKE '%;".$planete->id_user.";%' LIMIT 1;");
        if (!empty($ratifier)) {
            $ratifier["signatures"] = explode(';', substr($ratifier["signatures"], 1), -1);
            unset($ratifier["signatures"][array_search($planete->id_user, $ratifier["signatures"])]);
            if (count($ratifier["signatures"]) == 0) {
                $ratifier["signatures"] = "";
            } else {
                $ratifier["signatures"] = ';'.implode(';', $ratifier["signatures"]).';';
            }
            $bdd->escape($ratifier["signatures"]);
            $bdd->query("UPDATE $table_alliances_creation SET signatures = '".$ratifier["signatures"]."' WHERE id = ".$ratifier["id"].";");
        }

        $alliance = $bdd->unique_query("SELECT * FROM $table_alliances_creation WHERE id = $hash;");
        if (!empty($alliance["signatures"])) {
            $alliance["signatures"] = explode(';', substr($alliance["signatures"], 1), -1);
        } else {
            $alliance["signatures"] = array();
        }

        $alliance["signatures"][] = $planete->id_user;
        $newnb = count($alliance["signatures"]);
        $alliance["signatures"] = ';'.implode(';', $alliance["signatures"]).';';
        $bdd->escape($alliance["signatures"]);
        $bdd->query("UPDATE $table_alliances_creation SET signatures = '".$alliance["signatures"]."' WHERE id = $hash;");
        if (debut_d_univers) { //Si on est en début d'univers, on recherche une planète du joueur fondateur
            $planete_fondateur = $bdd->unique_query("SELECT id FROM $table_planete WHERE id_user = ".$alliance["fondateur"]." ORDER BY id ASC LIMIT 1;");
        }
        $bdd->deconnexion();
        if ($newnb >= nb_signatures) {
            if (debut_d_univers && $newnb == nb_signatures && empty($planete_fondateur->vaisseaux[2])) {
                //On donne un vaisseau de colonisation au fondateur
                $planete_fondateur = new Planete($planete_fondateur["id"]);
                $planete_fondateur->vaisseaux[2]++;
                unset($planete_fondateur);

                send_mp($alliance["fondateur"], "Nouvelle signature pour votre alliance !", $planete->pseudo." vient de ratifier votre alliance, portant ainsi à ".$newnb." le nombre de signatures.<br /><br />Vous avez suffisamment de signatures pour coloniser un astéroïde. Pour vous féliciter, le conseil intergalactique a décidé de vous offrir un vaisseau de colonisation pour aller coloniser un astéroide afin d'implanter votre alliance.");
            } else {
                //TODO refaire un message plus RP
                send_mp($alliance["fondateur"], "Nouvelle signature pour votre alliance !", $planete->pseudo." vient de ratifier votre alliance, portant ainsi à ".$newnb." le nombre de signatures.<br /><br />Vous avez suffisamment de signatures pour coloniser un astéroïde. Envoyez-en un dès maintenant !");
            }
        } else {
            send_mp($alliance["fondateur"], "Nouvelle signature pour votre alliance !", $planete->pseudo." vient de ratifier votre alliance, portant ainsi à ".$newnb." le nombre de signatures.");
        }

        erreur("Votre signature a bien été prise en compte !", "green");
    } else {
        redirection($VAR["menu"]["alliance"]);
    }
} else {
    $bdd->reconnexion();
    $bdd->escape($hash);
    $fonder = $bdd->unique_query("SELECT * FROM $table_alliances_creation WHERE fondateur = ".$planete->id_user." LIMIT 1;");
    if (!empty($fonder)) {
        $bdd->deconnexion();
        erreur("Vous ne pouvez pas ratifier d'alliance car vous en fonder actuellement une !", "red");
    }
    $alliance = $bdd->unique_query("SELECT * FROM $table_alliances_creation WHERE lien = '$hash';");
    $ratifier = $bdd->unique_query("SELECT * FROM $table_alliances_creation WHERE signatures LIKE '%;".$planete->id_user.";%' LIMIT 1;");
    $bdd->deconnexion();

    if (!empty($alliance) && sha1($alliance['tag'].'Hb$'.$alliance['nom_alliance'].'☺Ø'.$alliance['fondateur'].'‘«'.$planete->race) == $hash) {
        if (!empty($alliance["signatures"])) {
            $alliance["signatures"] = explode(';', substr($alliance["signatures"], 1), -1);
            if (in_array($planete->id_user, $alliance["signatures"])) {
                erreur("Vous avez déjà ratifier cette alliance !");
            }
            $alliance["nbsignatures"] = count($alliance["signatures"]);
        } else {
            $alliance["nbsignatures"] = 0;
        }
        $template->assign("alliance", $alliance);
        $template->assign("ratifier", $ratifier);
    } elseif (!empty($alliance)) {
        erreur("Impossible de ratifier cette alliance.<br />Vous n'êtes peut-être pas de la même race que le fondateur.", 'red', $VAR["menu"]["alliance"], 4000);
    } else {
        erreur("Vous ne pouvez plus signer pour cette alliance !", 'red', $_SERVER["HTTP_REFERER"], 4000);
    }
}
