<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$id = intval(gpc('postuler'));
$message = gpc('motivation', 'post');
$page = 'alliance/nm_postuler';

if (!empty($message)) {
    $bdd->reconnexion();
    $bdd->escape($message);
    $postul = $bdd->unique_query("SELECT id FROM $table_alliances_attente WHERE id_alliance = $id AND id_membre = ".$planete->id_user." LIMIT 1;");
    if (empty($postul)) {
        $bdd->query("INSERT INTO $table_alliances_attente (id_alliance, id_user, timestamp, message) VALUES ($id, ".$planete->id_user.", ".time().", '$message');");
    }
    $bdd->deconnexion();

    if (empty($postul)) {
        erreur("Votre demande d'adhésion a été envoyée avec succès, vous aurez une réponse dès qu'un dirigeant de l'alliance se connectera.", "green", $VAR["menu"]["alliance"]."&v=".$id, 5000);
    } else {
        redirection($VAR["menu"]["alliance"]."&postuler=".$id);
    }
} else {
    $bdd->reconnexion();
    $alliance = $bdd->unique_query("SELECT * FROM $table_alliances WHERE id = $id;");
    $postul = $bdd->unique_query("SELECT id FROM $table_alliances_attente WHERE id_alliance = $id AND id_membre = ".$planete->id_user." LIMIT 1;");
    $bdd->deconnexion();


    if (!empty($postul)) {
        erreur("Vous avez déjà une candidature en cours pour cette alliance !", "red", $VAR["menu"]["alliance"]."&v=".$id, 3000);
    } elseif (!empty($alliance)) {
        $template->assign("alliance", $alliance);
    } else {
        redirection($VAR["menu"]["alliance"]);
    }
}
