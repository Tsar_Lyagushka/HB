<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

    $idN = intval(gpc('n'));

    $flotteN = new Flotte($idN, false);
    $flotteN->load_planete();

    if ($flotteN->id_flotte == 0 || $flotteN->start_planete->id_user != $planete->id_user) {
        erreur('Impossible de trouver cette flotte !', "red", '?p=flotte', 1500);
    } else {
        //Changement du nom de la flotte
        if (!empty($_POST['nomflotte'])) {
            $nouvNom = gpc('nomflotte', 'post');
            $flotteN->nom = $nouvNom;
            $flotteN->addModifFlotte("nom");

            redirection("?p=flotte&n=".$idN);
        }

        //Annulation de la mission
        if (isset($_GET['a']) && isset($SESS->values['ret_fleet']) && $_GET['a'] == $SESS->values['ret_fleet']) {
            //Calcul du temps passé depuis le lancement de la flotte
            $tpsDD = time() - $flotteN->start_time;
            if ($flotteN->mission == '6') {
                erreur('Votre flotte est déjà en train de revenir !', 'red', '?p=flotte&n='.$idN, 1500);
            } elseif ($flotteN->end_time > $tpsDD) {
                $bdd->query("UPDATE $table_flottes SET mission = '6', effectue = '1', end_time = $tpsDD, end_galaxie = start_galaxie, end_ss = start_ss, end_position = start_position WHERE id_user = ".$planete->id_user." AND id = $idN;");
                redirection("?p=flotte&n=".$idN);
            } else {
                erreur('Impossible d\'annuler la mission, elle a déjà commencée.', "red", '?p=flotte&n='.$idN, 1500);
            }
        }
    
        $SESS->values['ret_fleet'] = md5('HB.fleet_'.rand().'☺ß☻');
        $SESS->put();
    
        $template->assign('ret_fleet', $SESS->values['ret_fleet']);
        $template->assign('flotte', $flotteN);
        $page = 'flotten';
    }

unset($idN, $flotteN, $nouvNom, $tpsDD);
