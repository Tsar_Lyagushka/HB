<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'flotte1';

    //Demande de mise à jour des tactiques par défaut
    $attaque = gpc('attaque', 'post');
    $defense = gpc('defense', 'post');
    if (is_numeric($attaque) && is_numeric($defense)) {
        $chang = false;
        if (($attaque != $planete->combatAT_tactique) && (($attaque == 1 && $planete->technologies[3] & 4096) || ($attaque == 2 && $planete->technologies[3] & 8192) || ($attaque == 3 && $planete->technologies[3] & 16384) || $attaque == 0)) {
            $planete->combatAT_tactique = $attaque;
            $planete->addModifUser('combatAT_tactique');
            $chang = true;
        }
        if (($defense != $planete->combatDE_tactique) && (($defense == 1 && $planete->technologies[3] & 4096) || ($defense == 2 && $planete->technologies[3] & 8192) || ($defense == 3 && $planete->technologies[3] & 16384) || $defense == 0)) {
            $planete->combatDE_tactique = $defense;
            $planete->addModifUser('combatDE_tactique');
            $chang = true;
        }
    
        if ($chang) {
            erreur('Tactiques mises à jour avec succès.', 'green', '?p=flotte', 1100);
        }
    }
    unset($attaque, $defense, $chang);

    //Affichage des flottes en cours dans la galaxie
    $bdd->reconnexion();
    if (SURFACE == "asteroide") {
        $flottes = $bdd->query("SELECT id FROM $table_flottes WHERE id_alliance = ".$planete->id.";");
    } else {
        $flottes = $bdd->query("SELECT id FROM $table_flottes WHERE id_user = ".$planete->id_user.";");
    }
    $bdd->deconnexion();

    //Extraction des flottes en cours
    if ($flottes) {
        foreach ($flottes as $key => $flotte) {
            $flottes[$key] = new Flotte($flotte['id']);
            $flottes[$key]->load_planete();
        }
        $template->assign('flottesEC', $flottes);
    }

    //Calcul du nombre de slot disponible et vérouillage de l'envoie si besoin
    if ($flottes) {
        $nbFlottes = count($flottes);
    } else {
        $nbFlottes = 0;
    }
    if (SURFACE == "asteroide") {
        if ($planete->batiments[2] == 1) {
            $slots = 1;
        } elseif ($planete->batiments[2] == 2) {
            $slots = 2;
        } elseif ($planete->batiments[2] == 3) {
            $slots = 4;
        } elseif ($planete->batiments[2] == 4) {
            $slots = 7;
        } elseif ($planete->batiments[2] == 5) {
            $slots = 14;
        } else {
            $slots = 0;
        }

        $slots -= $nbFlottes;
    } else {
        $slots = count($queryPlanetes)-$nbFlottes;
    }

    if ($slots > 0) {
        //$template->assign('action', '<input class="submit" name="envoie" type="submit" value="Envoyer flotte" /> <input class="submit" name="groupe" type="submit" value="Envoye groupé" />');
        $template->assign('action', '<input class="submit" name="envoie" type="submit" value="Ok" />');
    } else {
        $template->assign('action', '<span class="lack">Nombre de flottes maximum simultanées atteint</span>');
    }

    $template->assign('nbflotte', $nbFlottes);
    $template->assign('nbflottemax', $slots + $nbFlottes);

    //Affichage des flottes en préparation
    if (isset($SESS->values["prepFlottes"])) {
        $template->assign('flottesEP', $SESS->values["prepFlottes"]);
    }

unset($nbFlottes, $slots, $flottes, $flotte, $key);
