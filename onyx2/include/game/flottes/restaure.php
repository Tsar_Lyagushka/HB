<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$idPrep = gpc('c');
$template->assign('idPrep', $idPrep);
$template->assign('restaure', $SESS->values["prepFlottes"][$idPrep]);

foreach ($SESS->values["prepFlottes"][$idPrep]['vaisseaux'] as $key => $vaisseau) {
    //On vérifie qu'il y a suffisamment de vaisseaux sur la planète
    if ($planete->vaisseaux[$key] < $vaisseau) {
        erreur('Vous n\'avez pas assez de vaisseaux sur cette planète pour envoyer cette flotte !', "red", '?p=flotte');
    }
}

//Génération de la liste de mission possible avec les vaisseaux de la flotte
$missions = array();
if ($SESS->values["prepFlottes"][$idPrep]['type'] == 1) {
    $missions = array(6 => "Stationner", 7 => "Donner des vaisseaux", 1 => "Transporter");
}
if ($SESS->values["prepFlottes"][$idPrep]['vaisseaux'][2]) {
    $missions[2] = "Coloniser";
}
if ($planete->technologies[7]& 16 && $VAR["attaques"]) {
    $missions[3] = "Attaquer";
}
if ($SESS->values["prepFlottes"][$idPrep]['type'] == 1 && $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][3]) {
    $missions[4] = "Recycler";
}
if ($SESS->values["prepFlottes"][$idPrep]['type'] == 1 && ($SESS->values["prepFlottes"][$idPrep]['vaisseaux'][6] || $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][13])) {
    $missions[5] = "Espionner";
}

//S'il n'y a aucun choix de mission possible, on abandonne
if (count($missions) == 0) {
    erreur('Aucune mission disponible !', "red", '?p=flotte');
}
$template->assign('missions', $missions);

$template->assign('scripth', '<script src="js/prototype.js" type="text/javascript"></script>');
$template->assign('script', '<script type="text/javascript">document.getElementById(\'nom\').focus();</script><script src="js/flotte.js" type="text/javascript"></script>');
$page = 'flotte2';

//Récupération des destinations favorites et des colonies
$favoris = array();
include_once("Class/tinyplanete.php");
foreach ($planete->destinationsFavoris as $fav) {
    $fav_Planete = new TinyPlanete($fav);
    if (!empty($fav_Planete->nom_planete)) {
        $favoris[$fav_Planete->id] = $fav_Planete->nom_planete;
    } else {
        $favoris[$fav_Planete->id] = '['.$fav_Planete->galaxie.':'.$fav_Planete->ss.':'.$fav_Planete->position.']';
    }
}
$favorisC = array();
foreach ($queryPlanetes as $fav) {
    if (!empty($fav['nom_planete'])) {
        $favorisC[$fav['id']] = $fav['nom_planete'];
    } else {
        $favorisC[$fav['id']] = '['.$fav['galaxie'].':'.$fav['ss'].':'.$fav['position'].']';
    }
}
$template->assign('favoris', $favoris);
$template->assign('favorisColonies', $favorisC);

unset($idPrep, $missions, $fav, $vaisseau, $key, $favorisC, $favoris);
