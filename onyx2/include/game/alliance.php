<?php
if (!defined('ONYX')) {
    exit;
}

if (!empty($_GET['v'])) {
    include('game/alliances/voir.php');
} elseif (!empty($_GET['signer'])) {
    include('game/alliances/signer.php');
} elseif (!empty($planete->id_alliance)) {
    $SESS->values['idPlan'] = 0;
    $SESS->values['idAsteroide'] = $planete->id_alliance;
    $SESS->values['isolement'] = 0;
    $SESS->put();
    redirection("?p=accueil");
} elseif (!empty($_GET['postuler'])) {
    include('game/alliances/postuler.php');
} else {
    include('game/alliances/sans.php');
}
