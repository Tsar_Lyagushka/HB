<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'carte';
$titre = 'Carte spatiale';

//Récupération d'un numéro d'amas à regarder
if (isset($_GET['amas'])) {
    $g = intval(gpc('amas'));
} elseif (isset($_GET['galaxie'])) {
    $g = intval(gpc('galaxie'));
} else {
    $g = $planete->galaxie;
}

//Validation du numéro de l'amas
if ($planete->auth_level >= 5 && $g <= 0) {
    $g = 0;
} elseif ($g < 1) {
    $g = 1;
}
if ($g > $VAR['nb_amas']) {
    $g = 1;
}

//Récupération d'un numéro de système à regarder
if (isset($_GET['systeme'])) {
    $s = intval(gpc('systeme'));
} elseif (isset($_GET['ss'])) {
    $s = intval(gpc('ss'));
} else {
    $s = $planete->ss;
}

//Validation du numéro de système
if ($s < 1) {
    $s = 1;
}
if ($s > $VAR['nb_systeme']) {
    $s = $VAR['nb_systeme'];
}

//Calcul des prochains amas et systèmes
if ($s == 1 && $g > 1) {
    $Gmu = $g - 1;
    $Smu = $VAR['nb_systeme'];
} elseif ($s == 1) {
    $Gmu = 15;
    $Smu = $VAR['nb_systeme'];
} else {
    $Gmu = $g;
    $Smu = $s - 1;
}
if ($s == $VAR['nb_systeme']) {
    $Gpu = $g + 1;
    $Spu = 1;
} else {
    $Gpu = $g;
    $Spu = $s + 1;
}

//Erreur au cas où la zone soit trop lointaine par rapport au niveau de la technologie
//if (($planete->technologies[1] < 1 && ($g != $planete->galaxie || $s != $planete->ss)) || ($planete->technologies[1] < 2 && $g != $planete->galaxie))
//	erreur('Pour afficher cette zone de la carte spatiale, vous devez plus développer votre technologie ');

//Génération de la carte à afficher
$TEMP_carte = array();
for ($i = 1; $i <= $VAR['nb_planete']; $i++) {
    //Cas d'un système à astéroïde
    if ($s%5 == 2 && $i == ceil($VAR['nb_planete']/2)) {
        $bdd->reconnexion();
        $d = $bdd->unique_query("SELECT race, nom_asteroide, debris_met, debris_cri, nom_alliance, image, tag, fondateur FROM $table_alliances WHERE galaxie = $g AND ss = $s;");
        $bdd->deconnexion();
        if ($d) {
            $TEMP_carte[] = array('A', $d['nom_asteroide'], $d['debris_met'], $d['debris_cri'], $d['race'], $d['nom_alliance'], $d['tag'], $d['image'], 0);
        } else {
            $TEMP_carte[] = array('A');
        }
    }

    $bdd->reconnexion();
    $resultat = $bdd->query("SELECT id FROM $table_planete WHERE galaxie = $g AND ss = $s AND position = $i;");
    if ($resultat) {
        $d = $bdd->unique_query("SELECT P.nom_planete, P.image, P.debris_met, P.debris_cri, U.pseudo, U.race, U.politique, U.id_alliance, A.tag FROM $table_planete P INNER JOIN $table_user U ON U.id = P.id_user LEFT OUTER JOIN $table_alliances A ON A.id = U.id_alliance WHERE P.galaxie = $g AND P.ss = $s AND P.position = $i;");
        $bdd->deconnexion();

        $TEMP_carte[] = array($i, $d['nom_planete'], $d['debris_met'], $d['debris_cri'], $d['race'], $d['pseudo'], $d['tag'], $d['image'], $d['politique']);
    } else {
        $bdd->deconnexion();
        $TEMP_carte[] = array($i);
    }
}
$template->assign('carte', $TEMP_carte);
$template->assign('position', array($g, $s, $Gpu, $Spu, $Gmu, $Smu));

unset($TEMP_carte, $g, $s, $Gpu, $Spu, $Gmu, $Smu, $resultat, $d, $i);
