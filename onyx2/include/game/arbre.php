<?php
if (!defined('INDEX')) {
    header('Location: ./'.$VAR['first_page']);
    exit;
}
$page = 'arbre';
$titre = 'Arbre des technologies';

if (empty($_GET['q'])) {
    $_GET['q'] = '';
}
$template->assign('defaut', gpc('q'));

//Récupération et vérification de la race voulue ou définition d'une race par défaut
if (empty($_GET['r']) || (gpc('r') != 'humain' && gpc('r') != 'covenant')) {
    $_GET['r'] = $planete->race;
}

$race = gpc('r');
$template->assign('raceAff', $race);

if (SURFACE == "asteroide") {
    $TEMP_liste = array();
    foreach ($planete->batiments as $id => $batiment) {
        if (!empty($LANG[$race]["alli_batiments"]["noms_sing"][$id])) {
            $TEMP_liste[$id] = array(
                'niveau' => $batiment,
                'etat' => dAlliancesBatiments::needed($id, $planete, true)
            );
        }
    }
    $template->assign('batiments', $TEMP_liste);
} else {
    $TEMP_liste = array();
    foreach ($planete->batiments as $id => $batiment) {
        if (!empty($LANG[$race]["batiments"]["noms_sing"][$id])) {
            $TEMP_liste[$id] = array(
                'niveau' => $batiment,
                'etat' => dBatiments::needed($id, $planete, true)
            );
        }
    }
    $template->assign('batiments', $TEMP_liste);
}


function traiterBrancheTechnologie($TEMP_liste, $branche, $i)
{
    global $LANG, $planete;

    foreach ($i as $key => $id) {
        if (is_array($id)) {
            $TEMP_liste = traiterBrancheTechnologie($TEMP_liste, $branche, $id);
        } else {
            $niveau = dTechnologies::niveau_du_joueur($branche, $id, $planete);
            $niveau_max = dTechnologies::niveau_max($branche, $id, $planete, $LANG);

            $object = array($branche, $id);
            $TEMP_liste[$branche][$id] = array(
            'id' => $id,
            'branche' => $branche,
            'niveau' => $niveau,
            'niveau_max' => $niveau_max,
            'etat' => dTechnologies::needed($object, $planete, true)
        );
        }
    }
    return $TEMP_liste;
}

$TEMP_liste = array();
$branche = 0;
while ($branche < 9) {
    $TEMP_liste[$branche] = array();
    $TEMP_liste = traiterBrancheTechnologie($TEMP_liste, $branche, dTechnologies::type($branche, $planete->race));
    $branche++;
}
$template->assign('technologies', $TEMP_liste);

$TEMP_liste = array();
foreach ($planete->casernes as $id => $unite) {
    if (!empty($LANG[$race]["caserne"]["noms_sing"][$id])) {
        $TEMP_liste[$id] = array(
            'niveau' => $unite,
            'etat' => dCaserne::needed($id, $planete, true)
        );
    }
}
$template->assign('caserne', $TEMP_liste);

$TEMP_liste = array();
$id = 1;
$max_unite = count($LANG[$race]["terrestre"]["noms_sing"]);
while ($id <= $max_unite) {
    if (!empty($LANG[$race]["terrestre"]["noms_sing"][$id])) {
        $TEMP_liste[$id] = array(
            'niveau' => $planete->terrestres[$id],
            'etat' => dTerrestre::needed($id, $planete, true, $race)
        );
    }
    $id++;
}
$template->assign('unites', $TEMP_liste);

$TEMP_liste = array();
$id = 1;
$max_unite = count($LANG[$race]["vaisseaux"]["noms_sing"]);
while ($id <= $max_unite) {
    if (!empty($LANG[$race]["vaisseaux"]["noms_sing"][$id])) {
        $TEMP_liste[$id] = array(
            'niveau' => $planete->vaisseaux[$id],
            'etat' => dSpatial::needed($id, $planete, true, $race)
        );
    }
    $id++;
}
$template->assign('vaisseaux', $TEMP_liste);

unset($TEMP_liste, $id, $unite);
