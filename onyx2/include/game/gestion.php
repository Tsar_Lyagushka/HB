<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

//Si l'on est sur un astéroïde, on charge la page de gestion d'alliance !
if (SURFACE == "asteroide") {
    include("game/alliances/gestion.php");
} else {
    $page = 'gestion';
    $titre = 'Gestion';

    //Demande de changement de politique
    $politique = gpc('politique', 'post');
    if (is_numeric($politique)) {
        //On annule le changement politique si le dernier a eu lieu dans la semaine.
        if ($planete->politique_lastchange > time() - 604800) {
            erreur("La population de votre empire planètaire vient d'entrer dans une phase de révolution contre le changement de régime.<br />Pour ne pas perdre votre place au sommet de l'empire, le système politique n'a pas été changé.");
        }
        if (($planete->technologies[3] & 2048 && ($politique == 3 || $politique == 2 || $politique == 1) || $politique == 0) && $planete->politique != $politique) {
            $planete->politique = $politique;
            $planete->politique_lastchange = time();
            $planete->addModifUser("politique");
            $planete->addModifUser("politique_lastchange");
            erreur("Le changement politique a bien été pris en compte par la population de vos différentes planètes.", "green");
        } elseif ($planete->politique == $politique) {
            erreur("Ce système politique est actuellement en vigueur.", "orange");
        } else {
            erreur("Impossible de choisir ce système politique !");
        }
    }

    if (SURFACE == "planete") {
        $template->assign(
            'planeteEC',
            array(
            'id' => $planete->id,
            'metal' => $planete->metal,
            'cristal' => $planete->cristal,
            'hydrogene' => $planete->hydrogene,
            'energie' => $planete->energie,
            'population' => $planete->population,
            )
        );
    }

    $politiques = array('Anarchie');
    if ($planete->technologies[3] & 2048) {
        $politiques[] = 'Fascisme (Mouvement Frieden)';
        $politiques[] = 'Communisme (Mouvement Koslovic)';
        $politiques[] = 'Démocratie (Administration coloniale)';
    }
    $template->assign('politiques', $politiques);
    $template->assign('moraldetails', $politiques);

    unset($politiques, $politiques, $politique);
}
