<?php
/***************************************************************************
 *                             class.combat.php
 *                            -------------------
 *   begin                : Samedi 26 janvier 2008
 *   update               : Mercredi 4 juin 2008
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/

class Combat
{
    public $refflotte = 0;
    public $ATvais = array();
    public $ENvais = array();
    public $ENres = array('metal' => 0, 'cristal' => 0, 'hydrogene' => 0);
    public $ENdef = array();
    public $Ntour = 0;
    public $ATtactique = 0;
    public $ENtactique = 0;
    public $timestamp = 0;
    public $vaisContenu = 0;
    public $vaisContenuM = 0;
    public $vaisContenuC = 0;
    public $vaisContenuH = 0;
    public $pillage = array(0, 0, 0);

    public $debriM = 0;
    public $debriC = 0;

    /**
     * Constructeur
     * @param    array	$flotteAT	tableau SQL des vaisseaux envoyés par l'attaquant
     * @param    array	$flotteEN	tableau SQL de la planète du défenseur
     * @param    array	$defEN	tableau SQL de la planète du défenseur
     *
     * @return   void
     * @access   public
     */
    public function Combat($flotteAT, $flotteEN, $defEN, $tableTechno = array(0, 0))
    {
        include(_FCORE."hb_game/vars.php");
        //Génération des vaisseaux attaquants
        for ($i=1 ; $i<=12 ; $i++) {
            if ($flotteAT['vaisseau_'.$i] >= 1) {
                //Création des groupes
                $nbvais = $flotteAT['vaisseau_'.$i];
                $nbgroupes = floor(sqrt(ceil($nbvais/10)));
                $nbvaispgroupe = floor($nbvais/$nbgroupes);
                $nbrest = $nbvais - $nbvaispgroupe * $nbgroupes;

                if (isset($groupe)) {
                    unset($groupe);
                }
                $groupe = array();
                for ($j=0 ; $j < $nbgroupes ; $j++) {
                    if ($j == 0) {
                        $groupe[] = array($nbvaispgroupe + $nbrest, $nomvais_bc[$i-1] * (1 + $tableTechno[0]/10), $nomvais_pv[$i-1]);
                    } else {
                        $groupe[] = array($nbvaispgroupe, $nomvais_bc[$i-1] * (1 + $tableTechno[0]/10), $nomvais_pv[$i-1]);
                    }
                }
                $this->ATvais[] = array($i, $flotteAT['vaisseau_'.$i], $nbgroupes, $groupe, $nomvais_initiative[$i-1]);
            }
        }

        //Définition d'autres variables de la classe concernant la flotte
        $this->refflotte = $flotteAT['id'];
        $this->vaisContenu = $flotteAT['contenu_max'];
        $this->vaisContenuM = $flotteAT['contenu_metal'];
        $this->vaisContenuC = $flotteAT['contenu_cristal'];
        $this->vaisContenuH = $flotteAT['contenu_hydrogene'];
        $this->timestamp = $flotteAT['start_time'] + $flotteAT['end_time'];

        //Génération des vaisseaux défenseurs
        for ($i=1 ; $i<=12 ; $i++) {
            if ($flotteEN['vaisseau_'.$i] >= 1) {
                //Création des groupes
                $nbvais = $flotteEN['vaisseau_'.$i];
                $nbgroupes = floor(sqrt(ceil($nbvais/10)));
                $nbvaispgroupe = floor($nbvais/$nbgroupes);
                $nbrest = $nbvais - $nbvaispgroupe * $nbgroupes;

                if (isset($groupe)) {
                    unset($groupe);
                }
                $groupe = array();
                for ($j=0 ; $j < $nbgroupes ; $j++) {
                    if ($j == 0) {
                        $groupe[] = array($nbvaispgroupe + $nbrest, $nomvais_bc[$i-1] * (1 + $tableTechno[1]/10), $nomvais_pv[$i-1]);
                    } else {
                        $groupe[] = array($nbvaispgroupe, $nomvais_bc[$i-1] * (1 + $tableTechno[1]/10), $nomvais_pv[$i-1]);
                    }
                }
                $this->ENvais[] = array($i, $flotteEN['vaisseau_'.$i], $nbgroupes, $groupe, $nomvais_initiative[$i-1]);
            }
        }

        //Génération des défenses défenseurs
        for ($i=1 ; $i<=5 ; $i++) {
            if ($defEN['def_'.$i] >= 1) {
                //Création des groupes
                $nbvais = $defEN['def_'.$i];
                $nbgroupes = floor(sqrt(ceil($nbvais/10)));
                $nbvaispgroupe = floor($nbvais/$nbgroupes);
                $nbrest = $nbvais - $nbvaispgroupe * $nbgroupes;

                if (isset($groupe)) {
                    unset($groupe);
                }
                $groupe = array();
                for ($j=0 ; $j < $nbgroupes ; $j++) {
                    if ($j == 0) {
                        $groupe[] = array($nbvaispgroupe + $nbrest, $defense_bc[$i-1] * (1 + $tableTechno[1]/10), $defense_pv[$i-1]);
                    } else {
                        $groupe[] = array($nbvaispgroupe, $defense_bc[$i-1] * (1 + $tableTechno[1]/10), $defense_pv[$i-1]);
                    }
                }
                $this->ENdef[] = array($i, $defEN['def_'.$i], $nbgroupes, $groupe, $defense_initiative[$i-1]);
            }
        }
    }

    /**
     * Change la tactique de l'attaquant
     * @param    int	$tactique	numéro de la tactique choisie
     *
     * @return   void
     * @access   public
     */
    public function changerTactiqueAT($tactique)
    {
        $this->ATtactique = ceil($tactique);
    }

    /**
     * Change la tactique du défenseur
     * @param    int	$tactique	numéro de la tactique choisie
     *
     * @return   void
     * @access   public
     */
    public function changerTactiqueEN($tactique)
    {
        $this->ENtactique = ceil($tactique);
    }

    /**
     * Régénére les boucliers
     * @param    int	$pourcentage	pourcentage de régénération
     * @param    bool	$attaquant	régénére le bouclier de l'attaquant si true, sinon régénrére celui du défenseur
     * @param    bool	$retour	si true, renvoie true ou false si !le pourcentage a été consommé ou non, si false, retrourne ne nombre de pourcentage restant
     * @param    int	$blindage	niveau de la technologie blindage du joueur
     *
     * @return   float pourcentage non utilisé
     * @access   public
     */
    public function regenereBC($pourcentage, $attaquant, $retour = false, $blindage = 0)
    {
        include(_FCORE."hb_game/vars.php");
        if ($attaquant) {
            $count = count($this->ATvais);
            $enplus = 0;
            $norm = 0;
            for ($i=0 ; $i<$count ; $i++) {
                $type = $this->ATvais[$i][0]-1;
                $maxbc = $nomvais_bc[$type] * (1 + $blindage/10);
                $ajout = $maxbc*$pourcentage/100;

                $cntbc = count($this->ATvais[$i][3]);
                for ($j=0 ; $j<$cntbc ; $j++) {
                    $norm += $maxbc * $this->ATvais[$i][3][$j][0];

                    if ($this->ATvais[$i][3][$j][1] < $maxbc) {
                        $this->ATvais[$i][3][$j][1] += $ajout;
                    } else {
                        $enplus += $ajout * $this->ATvais[$i][3][$j][0];
                    }
                    if ($this->ATvais[$i][3][$j][1] > $maxbc) {
                        $enplus += ($this->ATvais[$i][3][$j][1] - $maxbc)*$this->ATvais[$i][3][$j][0];
                        $this->ATvais[$i][3][$j][1] = $maxbc;
                    }
                }
            }
            if ($retour) {
                if ($norm != 0 && $enplus/$norm == 1) {
                    return $pourcentage;
                } else {
                    return false;
                }
            } else {
                return $enplus/$norm;
            }
        } else {
            $count = count($this->ENvais);
            $enplus = 0;
            $norm = 0;
            for ($i=0 ; $i<$count ; $i++) {
                $type = $this->ENvais[$i][0]-1;
                $maxbc = $nomvais_bc[$type] * (1 + $blindage/10);
                $ajout = $maxbc*$pourcentage/100;

                $cntbc = count($this->ENvais[$i][3]);
                for ($j=0 ; $j<$cntbc ; $j++) {
                    $norm += $maxbc * $this->ENvais[$i][3][$j][0];
                    if ($this->ENvais[$i][3][$j][1] < $maxbc) {
                        $this->ENvais[$i][3][$j][1] += $ajout;
                    } else {
                        $enplus += $ajout * $this->ENvais[$i][3][$j][0];
                    }
                    if ($this->ENvais[$i][3][$j][1] > $maxbc) {
                        $enplus += ($this->ENvais[$i][3][$j][1] - $maxbc)*$this->ENvais[$i][3][$j][0];
                        $this->ENvais[$i][3][$j][1] = $maxbc;
                    }
                }
            }
            if ($norm != 0) {
                $return = $enplus/$norm;
            } else {
                $return = 0;
            }

            //Défenses
            $count = count($this->ENdef);
            $enplus = 0;
            $norm = 0;
            for ($i=0 ; $i<$count ; $i++) {
                $type = $this->ENdef[$i][0]-1;
                $maxbc = $defense_bc[$type] * (1 + $blindage/10);
                $ajout = $maxbc*$pourcentage/100;

                $cntbc = count($this->ENdef[$i][3]);
                for ($j=0 ; $j<$cntbc ; $j++) {
                    $norm += $maxbc * $this->ENdef[$i][3][$j][0];
                    if ($this->ENdef[$i][3][$j][1] < $maxbc) {
                        $this->ENdef[$i][3][$j][1] += $ajout;
                    } else {
                        $enplus += $ajout * $this->ENdef[$i][3][$j][0];
                    }
                    if ($this->ENdef[$i][3][$j][1] > $maxbc) {
                        $enplus += ($this->ENdef[$i][3][$j][1] - $maxbc)*$this->ENdef[$i][3][$j][0];
                        $this->ENdef[$i][3][$j][1] = $maxbc;
                    }
                }
            }
            if ($norm != 0) {
                $return = $enplus/$norm;
            } else {
                $return = 0;
            }
            if ($retour) {
                if ($norm != 0 && $enplus/$norm == 1) {
                    return $pourcentage;
                } else {
                    return false;
                }
            } else {
                return $return/2;
            }
        }
    }

    /**
     * Calcul la puissance d'attaque disponible
     * @param    int	$pourcentage	pourcentage de régénération
     * @param    bool	$attaquant	calcul les points de l'attaquant si true, sinon calcul pour le défenseur
     * @param    int	$armement	niveau de la technologie armement du joueur
     * @param    bool	$method	true pour utiliser la mèthode classique, false pour utiliser la méthode d'Apocalypse Joe
     *
     * @return   int points disponibles
     * @access   public
     */
    public function calcAttaque($pourcentage, $attaquant, $armement = 0, $method = false)
    {
        include(_FCORE."hb_game/vars.php");
        if ($method) {
            if ($attaquant) {
                $puissance = 0;
                $count = count($this->ATvais);
                for ($i=0 ; $i<$count ; $i++) {
                    $maxat = $nomvais_at[$this->ATvais[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ATvais[$i][1];
                }
                return $puissance;
            } else {
                $puissance = 0;
                $count = count($this->ENvais);
                for ($i=0 ; $i<$count ; $i++) {
                    $maxat = $nomvais_at[$this->ENvais[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ENvais[$i][1];
                }

                //Défenses
                $count = count($this->ENdef);
                for ($i=0 ; $i<$count ; $i++) {
                    $maxat = $defense_at[$this->ENdef[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ENdef[$i][1];
                }
                return $puissance;
            }
        } else {
            if ($attaquant) {
                //Calcul du pourcentage de chaque vaisseau adverse
                $vaisEff = array();
                $nbvais = 0;
                $countj = count($this->ENvais);
                $countd = count($this->ENdef);
                for ($i=0 ; $i<$countj ; $i++) {
                    $nbvais += $this->ENvais[$i][1];
                }
                for ($i=0 ; $i<$countj ; $i++) {
                    $vaisEff[$this->ENvais[$i][0]] = $this->ENvais[$i][1]/$nbvais;
                }

                $puissance = 0;
                $count = count($this->ATvais);
                for ($i=0 ; $i<$count ; $i++) {
                    if ($this->ATvais[$i][4] > $this->Ntour) {
                        continue;
                    }

                    $bonus = 0;
                    for ($j=0 ; $j<$countj ; $j++) {
                        $bonus += $nomvais_rf[$this->ATvais[$i][0]-1][$this->ENvais[$i][0]-1] * $vaisEff[$this->ENvais[$i][0]];
                    }
                    for ($j=0 ; $j<$countd ; $j++) {
                        $bonus += 1/$countd;
                    }
                    $maxat = $nomvais_at[$this->ATvais[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ATvais[$i][1] * $bonus;
                }
                return $puissance;
            } else {
                //Calcul du pourcentage de chaque vaisseau adverse
                $vaisEff = array();
                $nbvais = 0;
                $countj = count($this->ATvais);
                for ($i=0 ; $i<$countj ; $i++) {
                    $nbvais += $this->ATvais[$i][1];
                }
                for ($i=0 ; $i<$countj ; $i++) {
                    $vaisEff[$this->ATvais[$i][0]] = $this->ATvais[$i][1]/$nbvais;
                }

                $puissance = 0;
                $count = count($this->ENvais);
                for ($i=0 ; $i<$count ; $i++) {
                    if ($this->ENvais[$i][4] > $this->Ntour) {
                        continue;
                    }

                    $bonus = 0;
                    for ($j=0 ; $j<$countj ; $j++) {
                        $bonus += $nomvais_rf[$this->ENvais[$i][0]-1][$this->ATvais[$i][0]-1] * $vaisEff[$this->ATvais[$i][0]];
                    }
                    $maxat = $nomvais_at[$this->ENvais[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ENvais[$i][1] * $bonus;
                }

                //Défenses
                $count = count($this->ENdef);
                for ($i=0 ; $i<$count ; $i++) {
                    if ($this->ENdef[$i][4] > $this->Ntour) {
                        continue;
                    }

                    $maxat = $defense_at[$this->ENdef[$i][0]-1] * (1 + $armement/10);
                    $puissance += $maxat * $pourcentage/100 * $this->ENdef[$i][1];
                }
                return $puissance;
            }
        }
    }

    /**
     * Attaque les vaisseaux adverses
     * @param    int	$points	points d'attaque disponible pour l'attaque
     * @param    bool	$attaquant	attaque le défenseur si true, sinon attaque l'attaquant
     *
     * @return   void
     * @access   public
     */
    public function attaquerVais($points, $attaquant)
    {
        include(_FCORE."hb_game/vars.php");
        if ($attaquant) {
            while ($points > 0) {
                // Calcul du nombre de vaisseaux et défenses à attaquer
                $nbvais = 0;
                $nbgroupes = 0;
                $nb = count($this->ENvais);
                for ($i=0 ; $i<$nb ; $i++) {
                    $nbvais += $this->ENvais[$i][1];
                    $nbgroupes += $this->ENvais[$i][2];
                }
                $nb = count($this->ENdef);
                for ($i=0 ; $i<$nb ; $i++) {
                    $nbvais += $this->ENdef[$i][1];
                    $nbgroupes += $this->ENdef[$i][2];
                }

                //S'il ne reste plus de vaisseaux et de défenses, on arrête la boucle
                if ($nbvais <= 0 || $nbgroupes <= 0 || $points <= 0) {
                    break;
                }

                //Calcul du nombre de points qui sera enlevé par vaisseau ou défense
                $ppv = $points / $nbvais;
                $points = 0;

                //On lance l'attaque contre les vaisseaux
                for ($j=0 ; $j<$nbgroupes ; $j++) {
                    $k = rand(0, count($this->ENvais)-1);
                    $l = rand(0, count($this->ENvais[$k][3])-1);

                    $this->ENvais[$k][3][$l][1] -= $ppv;
                    if ($this->ENvais[$k][3][$l][1] < 0) {
                        $this->ENvais[$k][3][$l][2] -= abs($this->ENvais[$k][3][$l][1]);
                        $this->ENvais[$k][3][$l][1] = 0;
                        if ($this->ENvais[$k][3][$l][2] <= 0) {
                            $this->debriM += $this->ENvais[$k][3][$l][0] * $nomvais_md[$this->ENvais[$k][0]];
                            $this->debriC += $this->ENvais[$k][3][$l][0] * $nomvais_cd[$this->ENvais[$k][0]];
                            $this->ENvais[$k][1] -= $this->ENvais[$k][3][$l][0];
                            $this->ENvais[$k][2] --;
                            array_splice($this->ENvais[$k][3], $l, 1);
                            if (!count($this->ENvais[$k][3])) {
                                array_splice($this->ENvais, $k, 1);
                            }
                        }
                    }
                }

                //On lance l'attaque contre les défenses
                for ($j=0 ; $j<$nbgroupes ; $j++) {
                    $k = rand(0, count($this->ENdef)-1);
                    $l = rand(0, count($this->ENdef[$k][3])-1);

                    $this->ENdef[$k][3][$l][1] -= $ppv;
                    if ($this->ENdef[$k][3][$l][1] < 0) {
                        $this->ENdef[$k][3][$l][2] -= abs($this->ENdef[$k][3][$l][1]);
                        $this->ENdef[$k][3][$l][1] = 0;
                        if ($this->ENdef[$k][3][$l][2] <= 0) {
                            $this->debriM += $this->ENdef[$k][3][$l][0] * $nomvais_md[$this->ENdef[$k][0]];
                            $this->debriC += $this->ENdef[$k][3][$l][0] * $nomvais_cd[$this->ENdef[$k][0]];
                            $this->ENdef[$k][1] -= $this->ENdef[$k][3][$l][0];
                            $this->ENdef[$k][2] --;
                            array_splice($this->ENdef[$k][3], $l, 1);
                            if (!count($this->ENdef[$k][3])) {
                                array_splice($this->ENdef, $k, 1);
                            }
                        }
                    }
                }
            }
            return count($this->ENvais) + count($this->ENdef);
        } else {
            while ($points > 0) {
                // Calcul du nombre de vaisseaux et défenses à attaquer
                $nbvais = 0;
                $nbgroupes = 0;
                $nb = count($this->ATvais);
                for ($i=0 ; $i<$nb ; $i++) {
                    $nbvais += $this->ATvais[$i][1];
                    $nbgroupes += $this->ATvais[$i][2];
                }

                //S'il ne reste plus de vaisseaux et de défenses, on arrête la boucle
                if ($nbvais <= 0 || $nbgroupes <= 0 || $points <= 0) {
                    break;
                }

                //Calcul du nombre de points qui sera enlevé par vaisseau ou défense
                $ppv = $points / $nbvais;
                $points = 0;

                //On lance l'attaque
                for ($j=0 ; $j<$nbgroupes ; $j++) {
                    $k = rand(0, count($this->ATvais)-1);
                    $l = rand(0, count($this->ATvais[$k][3])-1);

                    $this->ATvais[$k][3][$l][1] -= $ppv;
                    if ($this->ATvais[$k][3][$l][1] < 0) {
                        $this->ATvais[$k][3][$l][2] -= abs($this->ATvais[$k][3][$l][1]);
                        $this->ATvais[$k][3][$l][1] = 0;
                        if ($this->ATvais[$k][3][$l][2] <= 0) {
                            $this->debriM += $this->ATvais[$k][3][$l][0] * $nomvais_md[$this->ATvais[$k][0]];
                            $this->debriC += $this->ATvais[$k][3][$l][0] * $nomvais_cd[$this->ATvais[$k][0]];
                            $this->ATvais[$k][1] -= $this->ATvais[$k][3][$l][0];
                            $this->ATvais[$k][2] --;
                            array_splice($this->ATvais[$k][3], $l, 1);
                            if (!count($this->ATvais[$k][3])) {
                                array_splice($this->ATvais, $k, 1);
                            }
                        }
                    }
                }
            }
            return count($this->ATvais);
        }
    }

    public function exportAT($pillage = false)
    {
        include(_FCORE."hb_game/vars.php");
        $nb = count($this->ATvais);
        $nbvais = 0;
        $vaisContenu = 0;
        $vaisseau_1 = 0;
        $vaisseau_2 = 0;
        $vaisseau_3 = 0;
        $vaisseau_4 = 0;
        $vaisseau_5 = 0;
        $vaisseau_6 = 0;
        $vaisseau_7 = 0;
        $vaisseau_8 = 0;
        $vaisseau_9 = 0;
        $vaisseau_10 = 0;
        $vaisseau_11 = 0;
        $vaisseau_12 = 0;
        for ($i=0 ; $i<$nb ; $i++) {
            ${'vaisseau_'.$this->ATvais[$i][0]} += $this->ATvais[$i][1];
            $nbvais += $this->ATvais[$i][1];
            $this->vaisContenu += $nomvais_rs[$this->ATvais[$i][0]-1];
        }
        $sommeCont = $this->vaisContenuM + $this->vaisContenuC + $this->vaisContenuH;
        if ($sommeCont > $this->vaisContenu) {
            $retirer = $sommeCont/$this->vaisContenu;
            $this->vaisContenuM = floor($this->vaisContenuM/$retirer);
            $this->vaisContenuC = floor($this->vaisContenuC/$retirer);
            $this->vaisContenuH = floor($this->vaisContenuH/$retirer);
        }
        if ($pillage) {
            $ressplus = pillage($this->ENres['metal'], $this->ENres['cristal'], $this->ENres['hydrogene'], $this->vaisContenu - $this->vaisContenuM - $this->vaisContenuC - $this->vaisContenuH);

            $this->vaisContenuM += $ressplus[0];
            $this->vaisContenuC += $ressplus[1];
            $this->vaisContenuH += $ressplus[2];
            $this->pillage = array($ressplus[0], $ressplus[1], $ressplus[2]);
        }
        return 'nb_vais = \''.$nbvais.'\', contenu_max = \''.$this->vaisContenu.'\', contenu_metal = \''.$this->vaisContenuM.'\', contenu_cristal = \''.$this->vaisContenuC.'\', contenu_hydrogene = \''.$this->vaisContenuH.'\', vaisseau_1 = \''.$vaisseau_1.'\', vaisseau_2 = \''.$vaisseau_2.'\', vaisseau_3 = \''.$vaisseau_3.'\', vaisseau_4 = \''.$vaisseau_4.'\', vaisseau_5 = \''.$vaisseau_5.'\', vaisseau_6 = \''.$vaisseau_6.'\', vaisseau_7 = \''.$vaisseau_7.'\', vaisseau_8 = \''.$vaisseau_8.'\', vaisseau_9 = \''.$vaisseau_9.'\', vaisseau_10 = \''.$vaisseau_10.'\', vaisseau_11 = \''.$vaisseau_11.'\', vaisseau_12 = \''.$vaisseau_12.'\'';
    }

    public function pillageSimul($metal, $cristal, $hydrogene)
    {
        $ressplus = pillage($metal, $cristal, $hydrogene, 999999);
        $this->pillage = array($ressplus[0], $ressplus[1], $ressplus[2]);
    }

    public function exportEN()
    {
        $nb = count($this->ENvais);
        $vaisseau_1 = 0;
        $vaisseau_2 = 0;
        $vaisseau_3 = 0;
        $vaisseau_4 = 0;
        $vaisseau_5 = 0;
        $vaisseau_6 = 0;
        $vaisseau_7 = 0;
        $vaisseau_8 = 0;
        $vaisseau_9 = 0;
        $vaisseau_10 = 0;
        $vaisseau_11 = 0;
        $vaisseau_12 = 0;
        for ($i=0 ; $i<$nb ; $i++) {
            ${'vaisseau_'.$this->ENvais[$i][0]} += $this->ENvais[$i][1];
        }
        $nb = count($this->ENdef);
        $def_1 = 0;
        $def_2 = 0;
        $def_3 = 0;
        $def_4 = 0;
        $def_5 = 0;
        for ($i=0 ; $i<$nb ; $i++) {
            ${'def_'.$this->ENdef[$i][0]} += $this->ENdef[$i][1];
        }
        return 'vaisseau_1 = \''.$vaisseau_1.'\', vaisseau_2 = \''.$vaisseau_2.'\', vaisseau_3 = \''.$vaisseau_3.'\', vaisseau_4 = \''.$vaisseau_4.'\', vaisseau_5 = \''.$vaisseau_5.'\', vaisseau_6 = \''.$vaisseau_6.'\', vaisseau_7 = \''.$vaisseau_7.'\', vaisseau_8 = \''.$vaisseau_8.'\', vaisseau_9 = \''.$vaisseau_9.'\', vaisseau_10 = \''.$vaisseau_10.'\', vaisseau_11 = \''.$vaisseau_11.'\', vaisseau_12 = \''.$vaisseau_12.'\', def_1 = \''.$def_1.'\', def_2 = \''.$def_2.'\', def_3 = \''.$def_3.'\', def_4 = \''.$def_4.'\', def_5 = \''.$def_5.'\'';
    }
}
