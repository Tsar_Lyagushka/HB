<?php
include_once("Class/user.php");
/***************************************************************************
 *                            class.SURFACE.php
 *                           -------------------
 *   begin                : Jeudi 21 août 2008
 *   update               : Dimanche 8 février 2009
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class SURFACE extends User
{
    public $id = 0;
    public $galaxie;
    public $ss;
    public $image;
    public $debris_met;
    public $debris_cri;
    public $metal;
    public $cristal;
    public $hydrogene;
    public $alert_ressources = array(false, false, false);
    public $timestamp;
    public $file_bat;
    public $file_vais;
    public $isolement = false;
    public $batiments = array();
    public $vaisseaux = array();
    public $modif = array();

    public function isolement()
    {
        return false;
    }

    public function addModif($modif)
    {
        if (!in_array($modif, $this->modif)) {
            $this->modif[] = $modif;
        }
    }
}
