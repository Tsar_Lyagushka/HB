<?php
/***************************************************************************
 *                            class.alliance.php
 *                           --------------------
 *   begin                : Vendredi 10 octobre 2008
 *   update               : Samedi 11 octobre 2008
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class Alliance extends SURFACE
{
    public $id;
    public $race;
    public $fondateur;
    public $sante;
    public $nom;
    public $tag;
    public $galaxie;
    public $ss;
    public $nom_asteroide;
    public $image_asteroide;
    public $debris_met;
    public $debris_cri;
    public $credits;
    public $metal;
    public $cristal;
    public $hydrogene;

    /**
     * Constructeur
     * @param    int	$id	id de l'alliance à importer
     *
     * @return   void
     * @access   public
     */
    public function Alliance($id = 0)
    {
        if (!empty($id)) {
            global $var___db, $config, $table_alliances;
            global $alli_batimentVAR, $spatialVAR;
            $bdd = new bdd();
            $bdd->connexion();
            $bdd->escape($id);
            $alli = $bdd->unique_query("SELECT * FROM $table_alliances WHERE id = $id;");
            $bdd->deconnexion();
            if (!empty($alli)) {
                $this->id = $alli["id"];
                $this->race = $alli["race"];
                $this->fondateur = $alli["fondateur"];
                $this->sante = $alli["sante"];
                $this->nom = $alli["nom"];
                $this->tag = $alli["tag"];
                $this->galaxie = $alli["galaxie"];
                $this->ss = $alli["ss"];
                $this->position = $alli["wing"];
                $this->nom_asteroide = $alli["nom_asteroide"];
                $this->image_asteroide = $alli["image_asteroide"];
                $this->debris_met = $alli["debris_met"];
                $this->debris_cri = $alli["debris_cri"];
                $this->credits = $alli["credits"];
                $this->metal = $alli["metal"];
                $this->cristal = $alli["cristal"];
                $this->hydrogene = $alli["hydrogene"];

                foreach ($alli_batimentVAR as $bat) {
                    $this->batiments[] = $alli[$bat];
                }
                $this->file_bat = unserialize($alli["file_bat"]);

                foreach ($spatialVAR as $vais) {
                    $this->vaisseaux[] = $plan[$vais];
                }
                $this->file_vais = unserialize($alli["file_vais"]);

                $this->actualiser();
            }
        }
    }

    /**
     * Actualise les ressources de la planète en fonction de la production et termine les files d'attentes.
     *
     * @return   void
     * @access   public
     */
    public function actualiser($actuFile = true)
    {
        //Actualisation des files d'attentes
        if ($actuFile) {
            $this->file_pret("alli_batiments");
            $this->file_pret("vaisseaux");
        }
    }

    /**
     * Destructeur
     *
     * @return   void
     * @access   public
     */
    public function __destruct()
    {
        global $var___db, $config, $table_alliances;
        $nb = count($this->modif);
        $out = array();
        $bdd = new bdd();
        $bdd->connexion();
        for ($i = 0; $i < $nb; $i++) {
            if (!is_array($this->{$this->modif[$i]})) {
                $bdd->escape($this->{$this->modif[$i]});
                if (is_int($this->{$this->modif[$i]}) || is_float($this->{$this->modif[$i]})) {
                    $out[] .= $this->modif[$i]." = ".$this->{$this->modif[$i]};
                } else {
                    $out[] .= $this->modif[$i]." = '".$this->{$this->modif[$i]}."'";
                }
            } else {
                if (preg_match('#file#', $this->modif[$i])) {
                    $prep = serialize($this->{$this->modif[$i]});
                    $bdd->escape($prep);
                    $out[] .= $this->modif[$i]." = '$prep'";
                } else {
                    if ($this->modif[$i] == "batiments") {
                        $calc = "batiment";
                    } elseif ($this->modif[$i] == "alli_batiments") {
                        $calc = "alli_batiment";
                    } elseif ($this->modif[$i] == "technologies") {
                        $calc = "technolo";
                    } elseif ($this->modif[$i] == "casernes") {
                        $calc = "casernen";
                    } elseif ($this->modif[$i] == "terrestres") {
                        $calc = "nomterrn";
                    } elseif ($this->modif[$i] == "vaisseaux") {
                        $calc = "nomvaisn";
                    } elseif ($this->modif[$i] == "coeff_bat") {
                        $calc = "coeff";
                    }

                    if (!isset(${$calc.'VAR'})) {
                        global ${$calc.'VAR'};
                    }

                    $nombr = count(${$calc.'VAR'});
                    for ($j = 0; $j < $nombr; $j++) {
                        $bdd->escape($this->{$this->modif[$i]}[$j]);
                        $out[] .= ${$calc.'VAR'}[$j]." = ".$this->{$this->modif[$i]}[$j]."";
                    }
                }
            }
        }
        if (!empty($out)) {
            $plan = $bdd->unique_query("UPDATE $table_alliances SET ".implode(', ', $out)." WHERE id = ".$this->id.";");
        }
        $bdd->deconnexion();
    }
}
