<?php

class Serveur
{

    /**
     * Constructeur
     *
     * @return   void
     * @access   public
     */
    public function Serveur()
    {
        return ;
    }

    public function classement($race = "all")
    {
        global $table_user;
        $bdd = new BDD();
        $get_race = "";
        if ($race === "humain" || $race === "covenant") {
            $get_race = "WHERE race = '$race'";
        }
        $classement = $bdd->query("SELECT id, pseudo, place_points, points, race FROM $table_user $get_race ORDER BY place_points ASC LIMIT 100;");
        $bdd->deconnexion();
        return $classement;
    }
}
