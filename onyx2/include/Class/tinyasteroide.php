<?php
class TinyAsteroide
{
    public $id = 0;
    public $galaxie;
    public $ss;
    public $nom_asteroide;

    /**
     * Constructeur
     * @param    int	$id	id de la planète à importer
     *
     * @return   void
     * @access   public
     */
    public function __construct($id)
    {
        //Récupération du nom des tables utilisées et connexion à la base de données
        global $table_alliances;
        $bdd = new BDD();

        //On traite le cas où l'on recoit l'ID ou les coordonnées de l'asteroide
        if (is_numeric($id)) {
            $aste = $bdd->unique_query("SELECT id, galaxie, ss, nom_asteroide FROM $table_alliances WHERE id = $id;");
            $bdd->deconnexion();
        } elseif (preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):?[Aa]?\]?$#', $id, $position)) {
            $aste = $bdd->unique_query("SELECT  id, galaxie, ss, nom_asteroide  FROM $table_alliances WHERE galaxie = ".$position[1]." AND ss = ".$position[2].";");
            $bdd->deconnexion();
        } else {
            trigger_error('Erreur #04 : Format de recherche d\'astéroide incorrect !', E_USER_ERROR);
        }

        if (!empty($aste)) {
            //Chargement des données depuis le résultat de la base de données
            $this->id = $aste["id"];
            $this->galaxie = $aste["galaxie"];
            $this->ss = $aste["ss"];
            $this->nom_asteroide = $aste["nom_asteroide"];
        }
    }
}
