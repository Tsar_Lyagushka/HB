<?php
if (!defined('ONYX')) {
    exit;
}

interface Donnees
{
    const image_covenant_default = "../covenant_na.jpg";
    const image_humain_default = "../humain_na.jpg";

    const coeff_demolition = 0.6;

    public static function metal($id, $niveau, surface $planete);
    public static function cristal($id, $niveau, surface $planete);
    public static function hydrogene($id, $niveau, surface $planete);
    public static function credits($id, $niveau, surface $planete);
    public static function temps($id, $niveau, surface $planete);

    public static function image($id, surface $planete);

    public static function needed($id, surface $planete, $print = false);
}

class dDonnees
{
    public static function tailleFile(surface $planete)
    {
        //On calcul la taille maximale de la file d'attente
        if (!empty($planete->technologies[1])) {
            if (($planete->technologies[1] &131072) == 131072) {
                return 5;
            } elseif (($planete->technologies[1] &65536) == 65536) {
                return 4;
            } elseif (($planete->technologies[1] &32768) == 32768) {
                return 3;
            } else {
                return 2;
            }
        } else {
            return 3;
        } //Au cas où il n'y ait pas de technologie sur le lieu, on fixe la taille de la file d'attente
    }


    public static function neededCheck($tableau, surface $planete)
    {
        if (!is_array($tableau)) {
            return true;
        } else {
            foreach ($tableau as $need) {
                switch ($need[0]) {
                    case 'batiments':
                        if (get_class($planete) == "Planete" && $planete->batiments[$need[1]] < $need[2]) {
                            return false;
                        }
                        break;
                    case 'batiments_max':
                        if (get_class($planete) == "Planete" && $planete->batiments[$need[1]] > $need[2]) {
                            return false;
                        }
                        break;
                    case 'technologies':
                        if (((int)$planete->technologies[$need[1]]& dTechnologies::idToBit($need[2])) == 0) {
                            return false;
                        }
                        break;
                    case 'casernes':
                        if ($planete->casernes[$need[1]] < $need[2]) {
                            return false;
                        }
                        break;
                    case 'terrestres':
                        if ($planete->terrestres[$need[1]] < $need[2]) {
                            return false;
                        }
                        break;
                    case 'vaisseaux':
                        if ($planete->vaisseaux[$need[1]] < $need[2]) {
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
            }
            return true;
        }
    }

    public static function print_neededCheck($tableau, surface $planete)
    {
        global $LANG;
        $race = $planete->race;
        if (!is_array($tableau)) {
            return 'Débloqué';
        } else {
            $return = '';
            foreach ($tableau as $need) {
                switch ($need[0]) {
                    case 'batiments':
                        if ($planete->batiments[$need[1]] < $need[2]) {
                            $return .= '<span class="lack">'.ucfirst($LANG[$race]["batiments"]["noms_sing"][$need[1]]).' niveau '.$need[2].' (batiment)</span><br />';
                        } else {
                            $return .= ucfirst($LANG[$race]["batiments"]["noms_sing"][$need[1]]).' niveau '.$need[2].' (batiment)<br />';
                        }
                        break;
                    case 'batiments_max':
                        if ($planete->batiments[$need[1]] > $need[2]) {
                            $return .= '<span class="lack">'.ucfirst($LANG[$race]["batiments"]["noms_sing"][$need[1]]).' < niveau '.($need[2]+1).' (batiment)</span><br />';
                        } else {
                            $return .= ucfirst($LANG[$race]["batiments"]["noms_sing"][$need[1]]).' < niveau '.($need[2]+1).' (batiment)<br />';
                        }
                        break;
                    case 'technologies':
                        if (((int)$planete->technologies[$need[1]]& dTechnologies::idToBit($need[2])) == 0) {
                            $return .= '<span class="lack">'.$LANG[$race]["technologies"]["noms_sing"][$need[1]][$need[2]].' (technologie)</span><br />';
                        } else {
                            $return .= $LANG[$race]["technologies"]["noms_sing"][$need[1]][$need[2]].' (technologie)<br />';
                        }
                        break;
                    case 'casernes':
                        if ($planete->casernes[$need[1]] < $need[2]) {
                            $return .= '<span class="lack">'.$need[2].' '.$LANG[$race]["caserne"]["noms_pluriel"][$need[1]].' (unité de la caserne)</span><br />';
                        } else {
                            $return .= $need[2].' '.$LANG[$race]["caserne"]["noms_pluriel"][$need[1]].' (unité de la caserne)<br />';
                        }
                        break;
                    case 'terrestres':
                        if ($planete->terrestres[$need[1]] < $need[2]) {
                            $return .= '<span class="lack">'.$need[2].' '.$LANG[$race]["terrestre"]["noms_pluriel"][$need[1]].' (unité du chantier terrestre)</span><br />';
                        } else {
                            $return .= $need[2].' '.$LANG[$race]["terrestre"]["noms_pluriel"][$need[1]].' (unité du chantier terrestre)<br />';
                        }
                        break;
                    case 'vaisseaux':
                        if ($planete->vaisseaux[$need[1]] < $need[2]) {
                            $return .= '<span class="lack">'.$need[2].' '.$LANG[$race]["vaisseaux"]["noms_pluriel"][$need[1]].' (unité du chantier spatial)</span><br />';
                        } else {
                            $return .= $need[2].' '.$LANG[$race]["vaisseaux"]["noms_pluriel"][$need[1]].' (unité du chantier spatial)<br />';
                        }
                        break;
                }
            }
            return $return;
        }
    }

    public static function nameVAR($name)
    {
        if ($name == "alli_batiments") {
            return "alli_batiments";
        } elseif ($name == "batiments") {
            return "batiments";
        } elseif ($name == "technologies") {
            return "technologies";
        } elseif ($name == "casernes") {
            return "caserne";
        } elseif ($name == "terrestres") {
            return "terrestre";
        } elseif ($name == "vaisseaux") {
            return "spatial";
        } elseif ($name == "coeff_bat") {
            return "coeff";
        }
    }


    public static function capaciteVilles(surface $planete)
    {
        return $planete->cases * pow($planete->batiments[17], -3) + 10;
    }

    public static function nameVilles($level)
    {
        switch ($level) {
            case 0:
            case 1:
            case 2:
                return "Poste d'observation taille ".($level+1);
                break;
            case 3:
            case 4:
            case 5:
                return "Poste avancé taille ".($level-2);
                break;
            case 6:
            case 7:
            case 8:
                return "Colonie taille ".($level-5);
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                return "Ville taille ".($level-8);
                break;
            case 13:
            case 14:
            case 15:
                return "Cité taille ".($level-12);
                break;
            case 16:
            case 17:
            case 18:
                return "Grande cité taille ".($level-15);
                break;
            case 19:
                return "Mégapole";
                break;
            case 20:
                return "Mégalopole";
                break;
            default:
                trigger_error("Taille de ville incorrect !");
        }
    }
}
