<?php
/**
 * PHPMailer language file.
 * English Version
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"] = 'You must provide at least one ' .
                                     'recipient email address.';
$PHPMAILER_LANG["mailer_not_supported"] = ' mailer is not supported.';
$PHPMAILER_LANG["execute"] = 'Could not execute: ';
$PHPMAILER_LANG["instantiate"] = 'Could not instantiate mail function.';
$PHPMAILER_LANG["authenticate"] = 'SMTP Error: Could not authenticate.';
$PHPMAILER_LANG["from_failed"] = 'The following From address failed: ';
$PHPMAILER_LANG["recipients_failed"] = 'Erreur SMTP: The following ' .
                                       'recipients failed: ';
$PHPMAILER_LANG["data_not_accepted"] = 'Erreur SMTP: Donn�es non accept�es.';
$PHPMAILER_LANG["connect_host"] = 'Erreur SMTP: Impossible de se connecter au serveur de mail.';
$PHPMAILER_LANG["file_access"] = 'Acc�s au fichier impossible: ';
$PHPMAILER_LANG["file_open"] = 'File Error: Ne peut ouvrir le fichier: ';
$PHPMAILER_LANG["encoding"] = 'Type d\'encodage inconnu : ';
