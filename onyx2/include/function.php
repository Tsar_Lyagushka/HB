<?php
function trouvNom($id_user)
{
    global $table_user;
    $bdd = new BDD();
    $resultat = $bdd->unique_query("SELECT pseudo FROM $table_user WHERE id = $id_user;");
    $bdd->deconnexion();
    return $resultat['pseudo'];
}

function linkNom($id_user)
{
    if (is_numeric($id_user)) {
        $nom = trouvNom($id_user);
    } else {
        $id_user = trouvId($id_user);
    }
    return '<a href="?p=util&amp;i='.$id_user.'">'.$nom.'</a>';
}

function trouvInfoUser($info, $type, $donnees)
{
    global $table_user;
    $bdd = new BDD();
    $resultat = $bdd->unique_query("SELECT ".implode(',', $donnees)." FROM $table_user WHERE $type = '$info';");
    $bdd->deconnexion();

    return $resultat;
}

function affTemp($secondes)
{
    $heures = intval($secondes/3600);
    $minutes = intval($secondes/60-($heures*60));
    if ($minutes < 10) {
        $minutes = '0'.$minutes;
    }
    $seconde = $secondes-($heures*3600)-($minutes*60);
    if ($seconde < 10) {
        $seconde = '0'.$seconde;
    }

    return $heures.':'.$minutes.':'.$seconde;
}

function sec($time)
{
    $output = '';
    $tab = array('jour' => '86400', 'heure' => '3600', 'minute' => '60', 'seconde' => '1');
    foreach ($tab as $key => $value) {
        $compteur = 0;
        while ($time > ($value-1)) {
            $time = $time - $value;
            $compteur++;
        }
        if ($compteur != 0) {
            $output .= $compteur.' '.$key;
            if ($compteur > 1) {
                $output .= 's';
            }
            if ($value != 1) {
                $output .= ' ';
            }
        }
    }
    if (empty($output)) {
        return 'Instantané';
    } else {
        return $output;
    }
}

function txtmission($mission)
{
    if ($mission == 0) {
        return 'Stationner';
    } elseif ($mission == 1) {
        return 'Transporter';
    } elseif ($mission == 2) {
        return 'Coloniser';
    } elseif ($mission == 3) {
        return 'Attaquer';
    } elseif ($mission == 4) {
        return 'Recycler';
    } elseif ($mission == 5) {
        return 'Espionner';
    } elseif ($mission == 6) {
        return 'Retour';
    } else {
        return 'Erreur';
    }
}

function pillage($metal, $cristal, $hydrogene, $vfm)
{
    $Qm = $metal / $vfm;
    $Qc = $cristal / $vfm;
    $Qh = $hydrogene / $vfm;

    $somme = $Qm + $Qc + $Qh;

    if ($somme < 2) {
        $Fm = $metal / 2;
        $Fc = $cristal / 2;
        $Fh = $hydrogene / 2;
    } else {
        $Fm = $Qm / $somme * $vfm;
        $Fc = $Qc / $somme * $vfm;
        $Fh = $Qh / $somme * $vfm;
    }

    return array(floor($Fm), floor($Fc), floor($Fh));
}

function send_mail($admail, $sujet, $corps)
{
    require_once("Class/phpmailer.php");

    $mail = new PHPmailer();
    $mail->SetLanguage('fr', ONYX."/include/Class/");
    $mail->IsSMTP();
    $mail->CharSet = "utf-8";
    $mail->Host='mail2.power-heberg.net';
    $mail->From='no-reply@anomaly-concepts.com';
    $mail->FromName='Halo-Battle';
    $mail->SMTPAuth=true;
    $mail->Username='no-reply@anomaly-concepts.com';
    $mail->Password='balamb468';

    $mail->AddAddress($admail);
    $mail->AddReplyTo('nemunaire@anomaly-concepts.com');
    $mail->Subject = $sujet;
    $mail->Body = $corps;

    $return = $mail->Send();
    $mail->SmtpClose();

    return $return;
}

function send_mailHTML($admail, $sujet, $corps)
{
    require_once("Class/phpmailer.php");

    $mail = new PHPmailer();
    $mail->SetLanguage('fr', ONYX."/include/Class/");
    $mail->IsSMTP();
    $mail->IsHTML(true);
    $mail->Host='mail2.power-heberg.net';
    $mail->From='no-reply@anomaly-concepts.com';
    $mail->FromName='Halo-Battle';
    $mail->SMTPAuth=true;
    $mail->Username='no-reply@anomaly-concepts.com';
    $mail->Password='balamb468';

    $mail->AddAddress($admail);
    $mail->AddReplyTo('nemunaire@anomaly-concepts.com');
    $mail->Subject = 'Halo-Battle :: '.$sujet;
    $mail->Body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><title>Halo-Battle :: '.$sujet.'</title></head><body><p>'.$corps.'<br /><br />A bient&ocirc;t dans Halo-Battle,<br />Le staff</p></body></html>';

    $return = $mail->Send();
    $mail->SmtpClose();

    return $return;
}

function erreur($message, $color = "red", $lien = "", $temps = 2500)
{
    global $template, $page, $SESS;

    if (!empty($page)) {
        $template->assign('page', $page);
    }

    $template->assign('message', $message);
    $template->assign('couleur', $color);

    if (!empty($lien)) {
        $template->assign('scripth', '<meta http-equiv="refresh" content="'.($temps/1000).'; url='.$lien.'" />');
    }

    //Si le joueur est connecté, on affiche la page d'erreur du jeu, sinon on afficher la page du cms
    if (isset($SESS->values['connected']) && $SESS->values['connected']) {
        $template->display('game/erreur.tpl');
    } else {
        $template->display('cms/erreur.tpl');
    }
    exit;
}

function send_mp($joueur, $titre, $message, $temps = 0, $emetteur = 0, $type = 0)
{
    global $table_mail, $VAR;
    if (empty($temps)) {
        $temps = time();
    }
    $mail = false;
    if (!is_numeric($joueur)) {
        $joueur = trouvInfoUser($joueur, "pseudo", array("id", "mail", "envoyerMail"));
        if (($emetteur == 0 && $joueur["envoyerMail"]& 1) || ($emetteur != 0 && $joueur["envoyerMail"]& 2)) {
            $mail = $joueur["mail"];
        } else {
            $mail = false;
        }

        $joueur = $joueur["id"];
    } else {
        $joueur = trouvInfoUser($joueur, "id", array("id", "mail", "envoyerMail"));
        if (($emetteur == 0 && $joueur["envoyerMail"]& 1) || ($emetteur != 0 && $joueur["envoyerMail"]& 2)) {
            $mail = $joueur["mail"];
        } else {
            $mail = false;
        }

        $joueur = $joueur["id"];
    }
    if (!is_numeric($emetteur)) {
        $emetteur = trouvInfoUser($emetteur, "pseudo", array("id"));
        $emetteur = $emetteur['id'];
    }
    if (!empty($joueur)) {
        $bdd = new BDD();
        $bdd->escape($joueur);
        $bdd->escape($titre);
        $bdd->escape($message);
        $bdd->query("INSERT INTO $table_mail (destinataire, expediteur, type, sujet, contenu, temps) VALUES($joueur, $emetteur, $type, '$titre', '$message', $temps);");
        $bdd->deconnexion();

        if ($mail) {
            if ($emetteur == 0) {
                send_mail($mail, "Halo-Battle :: Nouveau rapport", "Bonjour,\n\nVous recevez ce courriel suite à l'arrivée d'un nouveau rapport reçu sur votre compte de jeu du serveur ".$VAR['serveur_name'].". Le rapport concerne : \"".$titre."\". Vous pouvez utiliser le lien suivant pour voir le message ou vous connecter.\n\nhttp://".$_SERVER['HTTP_HOST']."/".$VAR["first_page"].$VAR["menu"]["messages"]."&n=recus\n\nVous pouvez désactiver ses notifications via les options de votre compte dans l'onglet \"notifications\"\n\nA bientôt dans Halo-Battle,\nLe Staff");
            } else {
                send_mail($mail, "Halo-Battle :: Nouveau message privé", "Bonjour,\n\nVous recevez ce courriel suite à l'arrivée d'un nouveau message privé reçu sur votre compte de jeu du serveur ".$VAR['serveur_name'].". Le sujet de ce message est : \"".$titre."\". Vous pouvez utiliser le lien suivant pour voir le message ou vous connecter.\n\nhttp://".$_SERVER['HTTP_HOST']."/".$VAR["first_page"].$VAR["menu"]["messages"]."&n=recus\n\nVous pouvez désactiver ses notifications via les options de votre compte dans l'onglet \"notifications\"\n\nA bientôt dans Halo-Battle,\nLe Staff");
            }
        }
    }
}

function gen_mdp($nbchar)
{
    $liste = "abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $code = "";
    for ($i = 0; $i < $nbchar; $i++) {
        $code .= $liste[rand(0, 60)];
    }

    return $code;
}

function check_mdp($mdp, $pseudo = null, $conf=null)
{
    if (isset($conf) && $conf !== $mdp) {
        return "Le mot de passe et sa confirmation sont différent";
    } elseif (strlen($mdp) < 7) {
        return "Mot de passe trop court, il doit faire au moins 7 caractères.";
    } elseif (ctype_digit($mdp)) {
        return "Le mot de passe ne doit pas contenir que des chiffres";
    } elseif (!empty($pseudo) && strpos($mdp, $pseudo)) {
        return "Votre mot de passe ne doit pas être une partie de votre nom d'utilisateur";
    } elseif (!empty($pseudo) && strpos($pseudo, $mdp)) {
        return "Votre nom d'utilisateur ne doit pas être une partie de votre mot de passe";
    } elseif (strpos($mdp, "halo")) {
        return "Le mot de passe ne peut pas contenir le mot halo !";
    } elseif (preg_match("#azer|qwer|zert|wert|erty|rtyu|tyui|yuio|uiop|poiu|oiuy|iuyt|uytr|ytre|trez|trew|reza|rewq|qsdf|asdf|sdfg|dfgh|fghj|ghjk|hjkl|jklm|mlkj|lkjh|kjhg|jhgf|hgfd|gfds|fdsq|fdsa|wxcv|vcxw|xcvb|bvcx|cvbn|nbvc|vbnm|mnbv|123|234|345|456|567|678|789|987|876|765|654|543|432|321|210|012#iU", $mdp, $osef)) {
        return "Ayez plus de créativité pour définir votre mot de passe, les lettres ou chiffres du clavier qui se suivent ne sont pas une bonne sécurité !";
    } else {
        return $mdp;
    }
}

function mdp($nom, $mdp, $alea = null)
{
    if (empty($alea)) {
        $alea = substr(random(1024), 0, 255);
        return array(hash('whirlpool', cxor(strtoupper($nom).':'.$mdp.'♂♪', $alea)), $alea);
    } else {
        return hash('whirlpool', cxor(strtoupper($nom).':'.$mdp.'♂♪', $alea));
    }
}

function redirection($url)
{
    header('Location: '.$url);
    print '<meta http-equiv="refresh" content="0; url='.$url.'" />';
    exit;
}

function slots($id_user)
{
    global $table_flottes, $table_planete;

    $bdd = new BDD();
    $flottes = $bdd->unique_query("SELECT COUNT(id) AS cnt FROM $table_flottes WHERE id_user = $id_user;");
    $colonies = $bdd->unique_query("SELECT COUNT(id) AS cnt FROM $table_planete WHERE id_user = $id_user;");
    $bdd->deconnexion();

    return (ceil(count($colonies["cnt"])/2 + 1) - $flottes["cnt"]);
}

function limite($txt, $limit)
{
    if (strlen($txt) > $limit) {
        return true;
    } else {
        return false;
    }
}

function bourse_calcPrixBase($dispo, $nb = 1000, $rate = 2)
{
    if ($nb > 1000) {
        $prix = 0;
        for ($i = 0; $i < $nb; $i+=1000) {
            $prix += bourse_calcPrixBase($dispo-$i, 1000, $rate);
        }
        $prix += bourse_calcPrixBase($dispo-$i, $dispo%1000, $rate);
        return $prix;
    } else {
        return ceil(pow($dispo, -0.1) * $nb * $rate);
    }
    //return pow($dispo, -0.1) * 200; //Prix de base ...
}
