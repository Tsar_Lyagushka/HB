<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Destinations rapides';

$a = gpc('a');
$amas = gpc('amas', 'post');
$ss = gpc('ss', 'post');
$pos = gpc('pos', 'post');

//Suppression d'une destination rapide
if (!empty($a)) {
    $d = gpc('d');
    if (!empty($d) && $a == md5(date('AG').'@'.$d)) {
        $f = array_keys($planete->destinationsFavoris, $d);
        unset($planete->destinationsFavoris[$f[0]]);
        $bdd->reconnexion();
        $bdd->query("UPDATE $table_user SET destinationsFavoris = '".serialize($planete->destinationsFavoris)."' WHERE id = $id_user;");
        $bdd->deconnexion();
    }
    header('Location: ?p=destinationsrapides');
    exit;
}
//Ajout d'une destination rapide
elseif (!empty($amas) && !empty($ss) && !empty($pos)) {
    $bdd->reconnexion();
    $plan = $bdd->unique_query("SELECT id FROM $table_planete WHERE galaxie = $amas AND ss = $ss AND position = $pos;");
    if ($plan) {
        if (in_array($plan['id'], $planete->destinationsFavoris)) {
            $bdd->deconnexion();
            erreur('Cette planète est déjà dans vos destinations rapides.', "red", '?p=destinationsrapides');
        }
        $planete->destinationsFavoris[] = $plan['id'];
        $bdd->query("UPDATE $table_user SET destinationsFavoris = '".serialize($planete->destinationsFavoris)."' WHERE id = $id_user;");
    } else {
        $bdd->deconnexion();
        erreur('Impossible d\'ajouter cette planète, elle n\'est pas habitée !', "red", '?p=destinationsrapides');
    }
    $bdd->deconnexion();

    header('Location: ?p=destinationsrapides');
    exit;
}
//Affichage de la liste des destinations rapides
else {
    $page = 'destinsrapid';

    $destins = array();
    $bdd->reconnexion();
    foreach ($planete->destinationsFavoris as $fav) {
        $res = $bdd->unique_query("SELECT nom_planete, galaxie, ss, position FROM $table_planete WHERE id = ".$fav.";");
        if ($res != false) {
            $destins[] = array($fav, $res['nom_planete'], '['.$res['galaxie'].':'.$res['ss'].':'.$res['position'].']', md5(date('AG').'@'.$fav));
        }
    }
    $bdd->deconnexion();
    $template->assign('destins', $destins);
}
unset($a, $amas, $ss, $pos, $destins, $res, $fav);
