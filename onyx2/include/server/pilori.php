<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'pilori';
$titre = 'Pilori de la galaxie';

if (isset($bdd)) {
    $bdd->reconnexion();
} else {
    $bdd = new BDD();
}
$pilori = $bdd->query("SELECT U.pseudo, U.operateurmv, 0 AS mail, U.mv, U.raisonmv, U.last_visite FROM $table_user U WHERE U.mv > 1 AND U.operateurmv = 0 UNION SELECT U.pseudo, O.pseudo AS operateurmv, O.mail, U.mv, U.raisonmv, U.last_visite FROM $table_user U INNER JOIN $table_user O ON U.operateurmv = O.id WHERE U.mv > 1;");
$bdd->deconnexion();

$template->assign('joueurs', $pilori);

unset($pilori);
