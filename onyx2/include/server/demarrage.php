<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'vide';

$bdd->reconnexion();
$message = $bdd->unique_query("SELECT * FROM $table_messages_demarrage ORDER BY time DESC LIMIT 1;");
$bdd->deconnexion();
$titre = $message['titre'];
$template->assign('contenu', '<p style="margin: 0 5px; text-align: center;">'.bbcode(nl2br($message['contenu']), 1).'<br /></p><h3><a class="submit" href="?p=accueil">Continuer</a></h3>');

unset($message);
