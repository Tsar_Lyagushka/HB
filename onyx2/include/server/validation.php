<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

if (!empty($_GET['i'])) {
    $BDD = new BDD();

    $id = $_GET['i'];
    $BDD->escape($id);

    $res = $BDD->unique_query("SELECT * FROM user_inscriptions WHERE id_activ = '$id'");
    if ($res) {
        switch ($res['serveur']) {
            case 0:
                $url = "http://127.0.0.1/hbn/";
                break;
            case 1:
                //$url = "http://battle.halo.fr/";
                //$url = "http://hb.s-fr.com/beta.php";
                //$url = "https://beta.halo-battle.fr/";
                $url = 'http://'.$_SERVER['HTTP_HOST'].'/';
                break;
            default:
                $url = "http://battle.halo.fr/";
        }

        //On envoie la requête vers le serveur
        $cds = sha1($res['pseudo'].'$'.$res['race'].'£'.$res['mdp'].'#'.$res['mail'].'ß'.$res['time_inscription'].'Ó'.$_SERVER['HTTP_USER_AGENT'].'♀☻'.$_SERVER['REMOTE_ADDR'].$res['placement']);
        header('Location: '.$url.'?p=njoueur&nom='.$res['pseudo'].'&race='.$res['race'].'&mdp='.strhex($res['mdp']).'&mail='.$res['mail'].'&ti='.$res['time_inscription'].'&placement='.$res['placement'].'&cds='.$cds);
        exit;
    } else {
        $template->assign('message', 'Le lien est expiré !');
        $template->assign('script', '<script type="text/javascript">setTimeout(\'document.location.href="?index";\', 2500);</script>');
        $template->assign('couleur', 'red');
        $template->display('cms/erreur.tpl');
        exit;
    }

    $BDD->deconnexion();
} else {
    header('Location: ./');
}
