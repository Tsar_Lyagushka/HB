<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'operateur';
$titre = 'Contact opérateur';

if (!empty($_POST['titre']) && !empty($_POST['description'])) {
    $titre = $_POST['titre'];
    $description = $_POST['description'];
    $time = time();

    $bdd->reconnexion();
    $bdd->escape($titre);
    $bdd->escape($description);
    $bdd->query("INSERT INTO $table_ope_mail (id_user, titre, contenu, time) VALUES ('$id_user', '$titre', '$description', '$time');");
    $bdd->deconnexion();
    erreur('Votre demande à bien été transmise aux opérateurs de la galaxie.<br />Une réponse vous sera donnée dans les plus brefs délais.', "green");
} elseif (!empty($_POST['O_titre']) && !empty($_POST['O_description']) && $SESS->level > 4) {
    $titre = $_POST['O_titre'];
    $description = $_POST['O_description'];

    $bdd->reconnexion();
    $bdd->escape($titre);
    $bdd->escape($description);
    $bdd->query("INSERT INTO $table_ope_faq (titre, contenu, ordre) VALUES ('$titre', '$description', 100);");
    $bdd->deconnexion();
    header('Location: ?p=operateur');
    exit;
} elseif (isset($_GET['a']) && $_GET['a'] == 'top' && isset($_GET['i']) && $SESS->level > 4) {
    $i = $_GET['i'];
    $bdd->reconnexion();
    $bdd->escape($i);
    $bdd->query("UPDATE $table_ope_faq SET ordre = ordre - 1 WHERE id = '$i' LIMIT 1;");
    $bdd->deconnexion();
    header('Location: ?p=operateur');
    exit;
} elseif (isset($_GET['a']) && $_GET['a'] == 'bas' && isset($_GET['i']) && $SESS->level > 4) {
    $i = $_GET['i'];
    $bdd->reconnexion();
    $bdd->escape($i);
    $bdd->query("UPDATE $table_ope_faq SET ordre = ordre + 1 WHERE id = '$i' LIMIT 1;");
    $bdd->deconnexion();
    header('Location: ?p=operateur');
    exit;
} elseif (isset($_GET['a']) && $_GET['a'] == 'del' && isset($_GET['i']) && $SESS->level > 4) {
    $i = $_GET['i'];
    $bdd->reconnexion();
    $bdd->escape($i);
    $bdd->query("DELETE FROM $table_ope_faq WHERE id = '$i' LIMIT 1;");
    $bdd->deconnexion();
    header('Location: ?p=operateur');
    exit;
}

$bdd->reconnexion();
$operateurs = $bdd->query("SELECT * FROM $table_user WHERE auth_level > 2 ORDER BY auth_level ASC;");
$questions = $bdd->query("SELECT * FROM $table_ope_faq ORDER BY ordre ASC;");
$bdd->deconnexion();

$template->assign('operateurs', $operateurs);

$TEMP_questions = array();
for ($i = 0; $i < $bdd->num_rows; $i++) {
    $TEMP_questions[] = array($questions[$i]['titre'], bbcode(nl2br(htmlspecialchars($questions[$i]['contenu']))), $questions[$i]['id']);
}
$template->assign('questions', $TEMP_questions);
