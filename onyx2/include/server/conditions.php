<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
if (!empty($SESS->values['connected'])) {
    $page = '../cms/vide';
} else {
    $page = 'vide';
}
$titre = 'Conditions générales d\'utilisation';

$template->assign('contenu', '
<h1>Conditions générales pour le site Halo-Battle.s-fr.com</h1>
<div style="text-align: left; margin: 15px; font-size: 115%;">
<br />
Halo, Bungie et Microsoft sont des marques et logos déposés par Microsoft Corporation.<br />
Tous droits réservés Copyright © 2006-2007<br />
halo-battle.s-fr.com Tous droits reservés.<br />
<br />
Le site d\'halo-battle.s-fr.com demande à tous les utilisateurs du jeu de lire et d\'accepter les Conditions Générales énoncées ci-dessous avant de s\'inscrire.<br />
<br />
<br />
Ces Conditions Générales d\'Utilisation ont été publiées le 23/04/2008 et sont applicables à partir du 30/04/2008.<br />
<br />
<br />
<a href="#services">I. Services</a><br />
<a href="#adhesion">II. Adhésion</a><br />
<a href="#protection">III. Protections des données</a><br />
<a href="#responsabilite">IV. Responsabilité</a><br />
<a href="#contenu">V. Contenu / responsabilité envers le contenu</a><br />
<a href="#actions">VI. Actions prohibées</a><br />
<a href="#restrictions">VII. Restrictions</a><br />
<a href="#droits">VIII. Droits de société concernant les comptes</a><br />
<a href="#modification">IX. Modification des Conditions Générales</a><br />
<a href="#loi">X. Loi applicable</a><br />
<br />
<br /><h3 id="services">I. Services</h3><br />
<h4>1 - Conditions de participation</h4><br />

Pour participer à Halo-Battle, l\'utilisateur doit accepter les Conditions Générales. Ces Conditions Générales sont principalement, mais non exclusivement, portées sur les services proposés par la présence en ligne de Halo-Battle.s-fr.com<br />

<br /><h4>2 - Responsabilité</h4><br />

Halo-Battle.s-fr.com est toujours attentif au bon fonctionnement de toutes les prestations qu\'il propose. Néanmoins, certaines desdites prestations peuvent ne pas être disponibles pour des circonstances indépendantes de notre volonté. Pour cette raison, l\'utilisateur ne peut avoir de halo-battle.s-fr.com ne peut être tenu pour responsable du mauvais fonctionnement du serveur ou pour un défaut de programmation. Les exemples précités ne sont pas exclusifs d\'autres possibilités.<br />

<br /><h3 id="adhesion">II. Adhésion</h3><br />

<br /><h4>1 - Conditions d\'adhésion</h4><br />
L\'adhésion commence à partir de l\'enregistrement d\'un compte sur le jeu ou le forum. L\'adresse e-mail utilisée pour cet enregistrement doit être valide. halo-battle.s-fr.com se réserve le droit de vérifier cette validité à n\'importe quel moment.<br />
Les utilisateurs reconnaissent disposer d\'une expérience suffisante en matière d\'utilisation d\'Internet et des fonctionnalités de communication électronique qui y sont liées.<br />
Les utilisateurs du site de halo-battle.s-fr.com sont tenus de s\'assurer de la compatibilité de leur logiciel de navigation avec le site de Halo-Battle.s-fr.com.<br />

<br /><h4>2 - Résiliation par l\'utilisateur</h4><br />
L\'adhésion peut être résiliée par l\'utilisateur par le biais de l\'effacement du compte. Cet effacement peut être retardé pour des questions d\'ordre technique. La gestion des informations concernant les données personnelles du compte est relatif au chapitre 3 des conditions générales d\'utilisation.<br />

<br /><h4>3 - Résiliation par Halo-Battle.s-fr.com</h4><br />
L\'utilisateur ne peut participer à la gestion des services proposés par Halo-Battle.s-fr.com. A ce titre, l\'équipe du jeu se réserve le droit d\'effacer ou de bloquer le compte de l\'utilisateur, en cas de violation des Conditions Générales, sans que cela ne soit exclusif d\'autres possibilités.<br />
L\'effacement du compte est décidé par l\'ensemble de l\'équipe. Les quelconques revendications concernant un effacement de compte peuvent être portées auprès de l\'équipe du jeu. Il ne peut y avoir de revendications légales concernant la clotûre du compte.<br />

<br /><h3 id="protection">III. Protections des données</h3><br />

<br /><h4>1 - Droit d\'accès aux données</h4><br />
Conformément à la loi du 7 janvier 1978, vous disposez d\'un droit d\'accès, de rectification, de vérification et de suppression relatif aux données vous concernant.<br />
Il vous suffit pour ce faire d\'adresser une demande par e-mail à contact@halo-battle.s-fr.com, Halo-Battle.s-fr.com modifiera et mettra ses informations à jour en conséquent.<br />

<br /><h4>2 - Stockage des données personnelles</h4><br />
Halo-Battle.s-fr.com se réserve le droit de stocker les données personnelles des utilisateurs dans le but de contrôler le respect des règles, des Conditions Générales et des dispositions législatives par lesdits utilisateurs. Les données personnelles concernées peuvent être les adresse IP par connexion, la méthode, l\'heure et les durées de connections, l\'adresse e-mail fournie lors de la création du compte ainsi que, le cas échéant, les données personnelles fournies volontairement par l\'utilisateur par le biais de son profil. Sur les forums, les données personnelles des profils des utilisateurs sont conservées.<br />

<br /><h4>3 - Communication et utilisation des données</h4><br />
Halo-Battle.s-fr.com se réserve le droit, en conformité avec les dispositions du droit français concernant la protection des données personnelles et toutes dispositions législatives applicables, de divulguer les données personnelles aux autorités, qui les requièrent en vue de protéger les intérêts des sociétés ou les éventuels manquements aux dispositions légales concernant la protection des données.

<br /><h4>4 - Opposition</h4><br />
L\'utilisateur peut s\'opposer au stockage de ses données personnelles à tout moment. La participation aux jeux nécessitant le stockage de ces données personnelles, le(s) compte(s) de l\'utilisateur sera(ont) supprimé(s) le plus rapidement possible en fonction des contraintes techniques.<br />

<br /><h3 id="responsabilite">IV. Responsabilité</h3><br />

Halo-Battle.s-fr.com n\'est pas responsable pour les dommages, quelle qu\'en soit la gravité, causés par l\'utilisation du jeu Halo-Battle. halo-battle.s-fr.com rappelle aux joueurs que l\'utilisation excessive des jeux informatiques peut causer de sévères troubles physiques.<br />

<br /><h3 id="contenu">V. Contenu / responsabilité envers le contenu</h3><br />

<br /><h4>1 - Utilisation de la plateforme des utilisateurs</h4><br />
Halo-Battle.s-fr.com fournit une plateforme qui permet à l\'utilisateur de communiquer avec chacun des autres utilisateurs. L\'utilisateur est responsable du contenu de ses propos : les propos pornographiques, xénophobes, insultants ou à contenu illégaux ne sont pas autorisés et relèvent de la responsabilité de leur auteur. En cas de violation, Halo-Battle.s-fr.com se réserve le droit d\'effacer ou de bloquer le compte de l\'utilisateur fautif et de l\'interdire d\'accès sur l\'ensemble des services proposés par l\'équipe. Ces sanctions ne sont pas exclusives d\'éventuelles poursuites judiciaires et/ou pénales.<br />

<br /><h4>2 - Dommages internes au contenu</h4><br />
L\'utilisateur du site est conscient du fait qu\'Internet n\'est pas un média parfaitement protégé et qu\'une sécurité totale quant au respect de la vie privée ne peut être garantie. Par conséquent, halo-battle.s-fr.com ne sera en aucune manière responsable des dommages éventuels subis par l\'utilisateur du site à la suite de l\'introduction par ce dernier d\'informations confidentielles ou délicates.<br />

<br /><h4>3 - Engagement de Halo-Battle.s-fr.com</h4><br />
halo-battle.s-fr.com s\'engage à mettre en oeuvre tous les moyens raisonnables à sa disposition en vue de permettre aux utilisateurs une utilisation optimale des services proposés. halo-battle.s-fr.com n\'est en conséquence tenue qu\'à une obligation de moyen, et en aucun cas à une obligation de résultat.<br />

halo-battle.s-fr.com ne peut être tenu pour responsable des conséquences de circonstances indépendantes de sa volonté telles que les grèves, lock-out, guerres, intempéries, etc., les défaillances des systèmes informatiques ou des moyens de communication, ou de tout autre événement constitutif d\'un cas de force majeure.<br />

<br /><h4>4 - Mise à disposition des informations</h4><br />
halo-battle.s-fr.com assure la mise à disposition via ce site d\'informations correctes et actualisées et se réserve donc le droit de modifier à tout moment le contenu du site. Toutefois, halo-battle.s-fr.com n\'est en aucun cas responsable des dommages causés à la suite de données éventuellement incorrectes, non actualisées ou incomplètes communiquées via le site.<br />

Les utilisateurs du site de halo-battle.s-fr.com s\'engagent à avertir immédiatement l\'équipe de toute utilisation frauduleuse du site de halo-battle.s-fr.com et à ne pas chercher à nuire à l\'intégrité ou au fonctionnement du site.<br />

<br /><h4>5 - Liens</h4><br />
halo-battle.s-fr.com n\'a aucun pouvoir de vérification et de contrôle quant à la création de liens vers des sites extérieurs et quant au contenu des dits sites, que ces sites soient marchands ou non.<br />

Les propriétaires des sites consultés à partir du site de Halo-Battle.s-fr.com sont, notamment, seuls responsables du respect de l\'ensemble des réglementations s\'appliquant dans le cadre des prestations offertes en ligne, et notamment, des lois et règlements relatifs à la vente à distance, la protection du consommateur, la publicité mensongère ou trompeuse, les prix, la conformité des produits, etc.<br />

Aussi, l\'utilisateur accepte que son choix d\'accéder à un autre site, par l\'intermédiaire d\'un lien hypertexte s\'effectue à ses risques et périls.<br />

En conséquence, tout préjudice direct ou indirect résultant de l\'accès à un site relié par un lien hypertexte ne peut engager la responsabilité de Halo-Battle.s-fr.com<br />

<br /><h3 id="actions">VI. Actions prohibées</h3><br />

<br /><h4>1 - Actions de manipulations</h4><br />
L\'utilisateur n\'a le droit d\'utiliser, un quelconque programme, mécanisme ou logiciel qui pourrait interférer avec les fonctions et/ou le développement du jeu. L\'utilisateur n\'a pas de le droit d\'effectuer une quelconque action qui causerait un ralentissement excessif des capacités techniques du site. L\'utilisateur n\'a pas le droit de bloquer, modifier ou reformuler le contenu créé par l\'équipe de dévellopement du projet.<br />

<br /><h4>2 - Programmes prohibés</h4><br />
Il est interdit de visualiser une quelconque partie du jeu avec un autre programme que les navigateurs Internet prévus à cet effet. Sont visés, tous autres programmes, en particulier ceux connus sous la dénomination de bots (sans que cette appellation soit exclusive), ainsi que tous
outils permettant de simuler, remplacer ou de suppléer le navigateur
internet. De la même manière, sont visés les scripts et les programmes
partiellement ou totalement automatiques qui peuvent procurer un
avantage par rapport aux autres utilisateurs. Les fonctions de
rafraîchissement automatique ("auto-refresh") et autres mécanismes
intégrés dans les navigateurs Internet sont également visés en tant
qu\'actions automatiques. L\'intégralité de ces mécanismes, sans que cela
soit exclusif d\'autres possibilités, est interdit. Le fait de bloquer la
publicité soit intentionnellement soit par le biais d\'un bloqueur de
pop-up voire par le biais d\'un module intégré aux navigateurs Internet
est sans conséquence sur cette interdiction. Les seules exceptions
possibles nécessitent la permission expresse de l\'équipe de Halo-Battle.s-fr.com<br />

<br /><h4>4 - Connexion directe</h4><br />
La connexion au compte de l\'utilisateur est uniquement permise par le
biais de la page d\'accueil du jeu. L\'ouverture automatique ou
automatisée, est interdite, que la page d\'accueil soit affichée ou non.<br />

<br /><h3 id="restrictions">VII. Restrictions</h3><br />

<br /><h4>1 - Nombre maximum de comptes</h4><br />
Chaque utilisateur n\'est autorisé qu\'à utiliser un compte par univers.
Sont dénommés "Multis" les utilisateurs qui n\'agissent pas en accord
avec cette règle. Les "Multis" sont susceptibles de voir leurs comptes
effacés ou bloqués sans avertissement préalable.<br />

<br /><h4>2 - Règles</h4><br />
Les particularités sont visées par les règles du jeu. Tous les
utilisateurs sont soumis à ces dispositions.<br />

<br /><h4>3 - Blocages</h4><br />
L\'utilisateur peut être bloqué de manière temporaire ou définitive. Le
blocage peut être valable sur une partie ou la totalité des services
proposés par Halo-Battle.s-fr.com<br />

<br /><h3 id="droits">VIII. Droits de société concernant les comptes</h3><br />

<br /><h4>1 - Généralités</h4><br />
L\'ensemble des comptes, incluant les ressources, unités, etc, sont des objets virtuels du jeu. L\'utilisateur n\'a pas la propriété ou tout autre type de droits sur le compte. L\'ensemble des droits sont de la propriété de Bungie et Microsoft Corporation. Aucun droit, en particulier ceux concernant l\'exploitation, ne peut être conféré à l\'utilisateur.<br />

<br /><h4>2 - Interdiction d\'exploitation</h4><br />
Il est interdit de contracter avec une tierce partie un quelconque accord concernant le transfert, l\'utilisation ou le stockage des comptes ou des données personnelles. Il est particulièrement interdit de vendre les comptes ou les ressources, voire de faire un quelconque profit en quittant son compte ou ses ressources au profit d\'un tiers. Les mêmes interdictions sont valables pour les données personnelles et les droits d\'utilisation. La violation de ces règles ou de tout autre droit de Bungie et Microsoft Corporation, particulièrement celles concernant le copyright, sera transmise aux autorités et sanctionnée par des poursuites judiciaires et/ou pénales.<br />

<br /><h4>3 - Exceptions</h4><br />
Il est permis de transférer les comptes de manière gratuite, ainsi que d\'échanger des ressources dans les limites permises par les règles du jeu.<br />

<br /><h3 id="modification">IX. Modification des Conditions Générales</h3><br />

Halo-Battle.s-fr.com se réserve le droit de modifier ou d\'étendre les Conditions Générales et les autres dispositions à tout moment. Cette modification ou extension sera publiée au minimum deux semaines avant l\'effectivité des changements.<br />

<br /><h3 id="loi">X. Loi applicable</h3><br />
La législation française est appliquée en cas de recours légal.<br />
</div>');
