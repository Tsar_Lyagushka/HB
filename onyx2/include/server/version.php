<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'version';
$titre = 'Historique des mises à jours';

$data = Cache::read('versionsDATA');
if (empty($data)) {
    $bdd->reconnexion();
    $data = $bdd->query("SELECT * FROM $table_version ORDER BY id DESC");
    $bdd->deconnexion();
        
    Cache::set('versionsDATA', $data);
}

$TEMP_versions = array();
foreach ($data as $version) {
    $TEMP_versions[] = array('numero' => $version['version'], 'date' => date("d/m/y", $version['temps']), 'description' => str_replace('<br />', '', $version['contenu']));
}
$template->assign('versions', $TEMP_versions);

unset($TEMP_versions, $data);
