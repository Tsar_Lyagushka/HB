<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Sanction joueur';
$pagea = 'erreur';

    $template->assign('linkpage', 'sjoueurs');

if (!empty($_GET['id']) && !empty($_GET['sanc']) && (!empty($_GET['raisonmv']) || $_GET['sanc'] < 0)) {
    $raisonmv = gpc('raisonmv');
    $id_plan = intval(gpc('id'));
    $sanc = gpc('sanc');
    $bdd = new BDD();
    $bdd->escape($raisonmv);
    
    if ($sanc == 'definitif' || $sanc== 'définitif' || $sanc == 'd') {
        $bdd->query("UPDATE $table_user SET mv = '3', raisonmv = '$raisonmv', operateurmv = $id_user WHERE id = $id_plan;");
        $bdd->deconnexion();

        $template->assign('message', 'Le joueur a été banni définitivement !');
    } else {
        $time = time() + $sanc * 86400 - 259200;
        $bdd->query("UPDATE $table_user SET mv = '2', last_visite = $time, raisonmv = '$raisonmv', operateurmv = $id_user WHERE id = $id_plan;");
        $bdd->deconnexion();

        $template->assign('message', 'Le joueur a été placé en mode vacances !');
    }
} elseif (!empty($_GET['name']) && !empty($_GET['sanc']) && isset($_GET['raisonmv'])) {
    $name = gpc('name');
    $bdd = new BDD();
    $bdd->escape($name);
    $req = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '$name';");
    $bdd->deconnexion();

    header('Location: '.$VAR["menu"]["sjoueur"].'&sanc='.gpc('sanc').'&raisonmv='.gpc('raisonmv').'&id='.$req['id']);
    exit;
} else {
    $pagea = 'sanctionU_choix';
}
