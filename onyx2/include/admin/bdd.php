<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

    if (!empty($_POST["req"])) {
        $req = stripslashes(gpc("req", "post"));
    } elseif (!empty($_GET["req"])) {
        $req = gpc("req");
    } else {
        $req = "SHOW TABLES;";
    }

    $bdd = new BDD();
    $template->assign("reponses", $bdd->query($req));
    $template->assign("erreur", $bdd->erreur());
    $template->assign("affected", $bdd->affected());
    $bdd->deconnexion();

    if (preg_match("#show tables#i", $req)) {
        $template->assign("lien", "SELECT * FROM ");
    }

$pagea = 'bdd';
$titre = 'Administration de la base de données';
