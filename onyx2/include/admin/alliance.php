<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'print';
$titre = 'Vérification alliance';

    $template->assign('linkpage', 'valliances');

if (!empty($_GET['id']) && !empty($_GET['key']) && $_GET['key'] != 'id') {
    $pagea = 'print_key';
    $id_plan = $_GET['id'];
    $key = $_GET['key'];
    $chapeau->connexion();
    $chapeau->escape($id_plan);
    $chapeau->escape($key);
    $reqA = $chapeau->unique_query("SELECT * FROM $table_alliances WHERE id = '$id_plan';");
    $req = $chapeau->unique_query("DESCRIBE $table_alliances $key;");
    $chapeau->deconnexion();
    $template->assign('tableau', $reqA);
    $template->assign('type', explode('(', $req['Type']));
    $template->assign('idPlan', $id_plan);
    $template->assign('key', $_GET['key']);
} elseif (!empty($_GET['id'])) {
    $id_plan = $_GET['id'];
    if (isset($_POST['key']) && isset($_POST['mod'])) {
        $key = $_POST['key'];
        $mod = $_POST['mod'];
        $chapeau->connexion();
        $chapeau->escape($mod);
        $chapeau->escape($id_plan);
        $chapeau->query("UPDATE $table_alliances SET $key = '$mod' WHERE id = '$id_plan';");
        $chapeau->deconnexion();
    }
    $chapeau->connexion();
    $req = $chapeau->unique_query("SELECT * FROM $table_alliances WHERE id = '$id_plan';");
    $chapeau->deconnexion();
    $template->assign('tableau', $req);
    $template->assign('idPlan', $id_plan);
} elseif (!empty($_GET['name'])) {
    $name = $_GET['name'];
    $chapeau->connexion();
    $chapeau->escape($name);
    $req = $chapeau->unique_query("SELECT * FROM $table_alliances WHERE nom = '$name';");
    $chapeau->deconnexion();
    header('Location: admin.php?p=valliances&id='.$req['id']);
    exit;
} else {
    $pagea = 'print_choixU';
}
