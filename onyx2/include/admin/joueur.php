<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'print';
$titre = 'V&eacute;rification joueur';

    $template->assign('linkpage', 'vjoueurs');

//Changement du nom d'utilisateur
if (!empty($_GET['id']) && !empty($_POST['key']) && !empty($_POST['mod']) && $_POST['key'] == 'pseudo') {
    $id_plan = intval(gpc('id'));
    $mod = gpc('mod', 'post');

    $liste = "abcdefghijklmnopqrstuvwxyz./!*123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $code = '';
    while (strlen($code) <= 8) {
        $code .= $liste[rand(0, 64)];
    }

    $mdp = mdp($mod, $code);

    $bdd = new BDD();
    $reqMail = $bdd->unique_query("SELECT mail FROM $table_user WHERE id = $id_plan;");
    $bdd->escape($mod);
    $reqPseudo = $bdd->query("SELECT mail FROM $table_user WHERE pseudo = '$mod';");
    if ($reqPseudo !== null) {
        erreur('Nom d\'utilisateur d&eacute;jà utilis&eacute;. Impossible de faire le changement !');
    }
    $bdd->query("UPDATE $table_user SET pseudo = '$mod', mdp = '".$mdp[0]."', mdp_var = '".$mdp[1]."' WHERE id = $id_plan;");
    $bdd->deconnexion();

    if (send_mail($reqMail['mail'], "Nouveau nom d'utilisateur", "Bonjour ".$mod.",\n\nVous recevez ce mail suite au changement de votre nom d'utilisateur sur le serveur ".$VAR["serveur_name"].".\nCe changement a été effectué par un opérateur de la galaxie, sur votre demande ou dans le cadre des règles du jeu.\nDe plus, pour votre sécurité, un nouveau mot de passe vous a été automatiquement attribué.\n\nVos nouvelles informations personnelles sont les suivantes :\n\tNom d'utilisateur : ".$mod."\n\tMot de passe : '.$code.'\n\nVotre ancien nom d'utilisateur et mot de passe ne sont plus valide à partir de maintenant et vous devez utiliser les données ci-dessus pour vous connecter.\n\nSi vous rencontrez des problèmes suites à ce changement, n'hésitez pas à contacter l'opérateur qui suivit votre demande.\n\nPour votre sécurité, nous vous rappelons qu'il est dangeureux de conserver un mail contenant des données personnelles. Pensez donc à supprimer ce mail une fois que vous vous serez connecté au jeu.\n\nL'équipe d'Halo-Battle")) {
        erreur("Nom d'utilisateur mis à jour avec succès. Un mail a &eacute;t&eacute; envoy&eacute; à l'utilisateur contenant un nouveau mot de passe.", "green");
    } else {
        erreur("Une erreur est survenue lors de l'envoie du mail.");
    }
}
//Changement du mot de passe
elseif (!empty($_GET['id']) && !empty($_POST['key']) && !empty($_POST['mod']) && $_POST['key'] == 'mdp') {
    $id_plan = intval(gpc('id'));
    $mod = gpc('mod', 'post');

    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT pseudo, mdp_var FROM $table_user WHERE id = $id_plan;");
    $mdp = mdp($req["pseudo"], $mod, $req["mdp_var"]);

    $bdd->query("UPDATE $table_user SET mdp = '$mdp' WHERE id = $id_plan;");
    $bdd->deconnexion();

    erreur("Mot de passe changé avec succès.<br />Pensez à avertir l'utilisateur de ce changement !", "green");
} elseif (!empty($_GET['id']) && !empty($_GET['key']) && $_GET['key'] != 'id' && $_GET['key'] != 'mdpNOUV' && $_GET['key'] != 'auth_level' && $_GET['key'] != 'mdp_var') {
    $pagea = 'print_key';
    $id_plan = intval(gpc('id'));
    $key = gpc('key');
    $bdd = new BDD();
    $bdd->escape($key);
    $reqJ = $bdd->unique_query("SELECT * FROM $table_user WHERE id = $id_plan;");
    $req = $bdd->unique_query("DESCRIBE $table_user $key;");
    $bdd->deconnexion();

    $template->assign('tableau', $reqJ);
    $template->assign('type', explode('(', $req['Type']));
    $template->assign('idPlan', $id_plan);
    $template->assign('key', $key);
} elseif (!empty($_GET['id'])) {
    $id_plan = intval(gpc('id'));
    if (isset($_POST['key']) && isset($_POST['mod'])) {
        $key = gpc('key', 'post');
        $mod = gpc('mod', 'post');
        $bdd = new BDD();
        $bdd->escape($mod);
        $bdd->escape($key);
        $bdd->query("UPDATE $table_user SET $key = '$mod' WHERE id = $id_plan;");
        $bdd->deconnexion();
    }
    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT * FROM $table_user WHERE id = $id_plan;");
    $bdd->deconnexion();

    $template->assign('tableau', $req);
    $template->assign('idPlan', $id_plan);
} elseif (!empty($_GET['name'])) {
    $name = gpc('name');
    $bdd = new BDD();
    $bdd->escape($name);
    $req = $bdd->unique_query("SELECT * FROM $table_user WHERE pseudo = '$name';");
    $bdd->deconnexion();
    header('Location: admin.php?p=vjoueurs&id='.$req['id']);
    exit;
} else {
    $pagea = 'print_choixU';
}
