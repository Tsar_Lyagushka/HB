<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'vip';
$titre = 'Vérification IP';

$timelimit = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y"))); //2008-06-17 18:15:00
$page = gpc('v');
if (empty($page) || !is_numeric($page)) {
    $page = 0;
}

$act = gpc('act');
if ($act == 'multiok') {
    $bdd = new BDD();
    $user = intval(gpc('util'));
    $bdd->query("UPDATE $table_user SET multi = '1' WHERE id = ".$user.";");
    $bdd->deconnexion();
    unset($user);
}

$trace = 'absent pour plus de rapidité';

$bdd = new BDD();
if (isset($_GET['ip'])) {
    $req = $bdd->query("SELECT R.id, R.id_util, R.time, R.ip, U.pseudo, U.mv, U.multi FROM $table_registre_identification R INNER JOIN $table_user U ON R.id_util = U.id WHERE ip = '".gpc('ip')."' ORDER BY ip ASC;");
    $trace = gethostbyaddr($req[0]['ip']);
} elseif (isset($_GET['util']) && isset($_GET['comp'])) {
    $req = $bdd->query("SELECT R.id, R.id_util, R.time, R.ip, U.pseudo, U.mv, U.multi FROM $table_registre_identification R INNER JOIN $table_user U ON R.id_util = U.id WHERE id_util = ".intval(gpc('util'))." OR id_util = ".intval(gpc('comp'))." ORDER BY ip ASC;");
} elseif (isset($_GET['util'])) {
    $req = $bdd->query("SELECT R.id, R.id_util, R.time, R.ip, U.pseudo, U.mv, U.multi FROM $table_registre_identification R INNER JOIN $table_user U ON R.id_util = U.id WHERE id_util = '".intval(gpc('util'))."' ORDER BY ip ASC;");
} elseif (isset($_GET['tri'])) {
    $req = $bdd->query("SELECT R.id, R.id_util, R.time, R.ip, U.pseudo, U.mv, U.multi FROM $table_registre_identification R INNER JOIN $table_user U ON R.id_util = U.id ORDER BY ".gpc('tri')." ASC;");
} else {
    $req = $bdd->query("SELECT R.id, R.id_util, R.time, R.ip, U.pseudo, U.mv, U.multi FROM $table_registre_identification R INNER JOIN $table_user U ON R.id_util = U.id WHERE R.time > '$timelimit 00:00:00' ORDER BY R.ip ASC LIMIT ".($page*75).",75;");
}
//SELECT last_ip,COUNT(*) FROM user GROUP BY last_ip HAVING COUNT(*)>1
$nbpage = $bdd->unique_query("SELECT COUNT(id) AS nb FROM $table_registre_identification WHERE time > '$timelimit 00:00:00';");
$bdd->deconnexion();

$anc = array('ip' => 0, 'id_util' => 0);
$tableau = array();
if (isset($resultat)) {
    foreach ($req as $resultat) {
        if ($resultat['mv'] == 3) {
            $color = 'DFBF00';
        } elseif ($resultat['multi'] == 1 && $anc['ip'] == $resultat['ip'] && $anc['id_util'] != $resultat['id_util']) {
            $color = 'EE66EE';
        } elseif ($anc['ip'] == $resultat['ip'] && $anc['id_util'] != $resultat['id_util']) {
            $color = 'FF0000';
        } else {
            $color = false;
        }

        $tableau[] = array($resultat['ip'], $trace, $resultat['id_util'], $resultat['pseudo'], $resultat['time'], $resultat['mv'], $color);
        $anc = $resultat;
    }
}
$template->assign('ips', $tableau);
$template->assign('numpage', $page);
$template->assign('nbpage', floor($nbpage['nb']/75));
