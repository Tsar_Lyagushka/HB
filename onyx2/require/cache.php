<?php

class Cache
{
    public function set($id, $var)
    {
        $file = '<?php $cache = '.var_export($var, true).'; ?>';
                
        file_put_contents(ONYX.'cache/'.md5($id).'.cache.php', $file) or trigger_error('dossier cache inaccessible en écriture.', E_USER_ERROR);
    }
        
    public function read($id)
    {
        if (!is_readable(ONYX.'cache/'.md5($id).'.cache.php')) {
            return false;
        }
                
        include(ONYX.'cache/'.md5($id).'.cache.php');
        if (!$cache) {
            return false;
        }
        return $cache;
    }
        
    public function del($id)
    {
        if (!is_file(ONYX.'cache/'.md5($id).'.cache.php')) {
            return false;
        }
        return unlink(ONYX.'cache/'.md5($id).'.cache.php');
    }
        
    public function flush()
    {
        foreach (glob(ONYX.'cache/*.cache.php') as $file) {
            unlink($file);
        }
    }
}
