<?php
define('IN_PHPBB', true);
$phpbb_root_path = $OPT['dir_forum'];
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
$user->session_begin();
$auth->acl($user->data);
$user->setup();

unset($phpbb_root_path, $phpEx);
