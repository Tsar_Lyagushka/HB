<?php

if (!defined('ONYX')) {
    exit;
}

if (!is_readable(ONYX.'lang/'.$OPT['type'].'.xml')) {
    trigger_error('Fichier de langue introuvable', E_USER_ERROR);
}

define('LANG', $OPT['type']);

$langdoc = Cache::read('traduction'.LANG);
if ($langdoc['check'] != md5_file(ONYX.'lang/'.LANG.'.xml')) {
    $langdoc = new DOMDocument();
        
    $langdoc->load(ONYX.'lang/'.LANG.'.xml') or trigger_error('Erreur du fichier de langue', E_USER_ERROR);
    $langdoc->normalizeDocument();
        
    $LANG = parse_config($langdoc->documentElement);
        
    Cache::set('traduction'.LANG, array(LANG => $LANG, 'check' => md5_file(ONYX.'lang/'.LANG.'.xml')));
} else {
    $LANG = $langdoc[LANG];
}

unset($langdoc);
