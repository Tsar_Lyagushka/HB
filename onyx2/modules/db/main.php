<?php

if (!defined('ONYX')) {
    exit;
}

switch ($OPT['type']) {
        case 'mysql':
        case 'postgresql':
            
            $api = array('mysql' => 'mysqli_connect', 'postgresql' => 'pg_connect');
            if (!function_exists($api[$OPT['type']])) {
                trigger_error('API introuvable', E_USER_ERROR);
            }
            unset($api);
            
            function dbpass($crypt, $cle)
            {
                return cxor(base64_decode($crypt), md5($cle, true));
            }
            
            $db_config = $OPT;
            
            require_once($OPT['type'].'.class.php');
            define('DB_TYPE', $OPT['type']);
            break;
        
        default: trigger_error('Base de donnee inconnue', E_USER_ERROR);
    }
