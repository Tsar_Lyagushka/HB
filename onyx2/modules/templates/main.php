<?php

if (!defined('ONYX')) {
    exit;
}

define('SMARTY_DIR', ONYX.'modules/templates/smarty/');

require_once(SMARTY_DIR."Smarty.class.php");

define('_TEMPLATE_DIR', ONYX.'tpl/'.$OPT['tpl'].'/');
define('_TEMPLATE_COMPILE', ONYX.$OPT['compile']);
define('_TEMPLATE_CONFIG', ONYX.$OPT['config']);
define('_TEMPLATE_CACHE', ONYX.$OPT['cache']);

class Template extends Smarty
{
    public $template_dir = _TEMPLATE_DIR;
    public $compile_dir = _TEMPLATE_COMPILE;
    public $config_dir = _TEMPLATE_CONFIG;
    public $cache_dir = _TEMPLATE_CACHE;

    public $compile_check = false;
    public $force_compile = true;
        
    public function set_template($i = null)
    {
    }
}
