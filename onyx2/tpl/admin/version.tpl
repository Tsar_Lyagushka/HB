{include file='game/header.tpl'}
			<h2>Gestion du cache</h2>
			<a href="{$menu.versions}&amp;actuCache">Mettre à jour le cache</a>
			<h2>{if $id == "add"}Ajouter{else}Modifier{/if} une version{if $id != "add"} : {$id}{/if}</h2>
			<form action="{$menu.versions}&amp;i={$id}" method="post">
				<fieldset class="options">
					<label for="version">Version :</label> <input type="text" name="version" id="version" value="{$mod.version}" tabindex="1" /><br />
					<label for="contenu">Contenu :</label><br /><textarea cols="10" rows="10" name="contenu" id="contenu" tabindex="2">{$mod.contenu}</textarea><br />
					<input type="submit" class="submit" value="GO" tabindex="3" />
				</fieldset>
			</form>
			<h2>Liste des versions</h2>
			<table>
				<thead>
					<tr>
						<th>Action</th>
						<th>Version</th>
						<th>Infos</th>
						<th>Date</th>
						<th>Par</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$tableau item=ligne key=key}
					<tr>
						<td><a href="{$menu.versions}&amp;i={$ligne.id}">EDIT</a> | <a href="{$menu.versions}&amp;d={$ligne.id}">SUPPR</a></td>
						<td>{$ligne.version}</td>
						<td>{$ligne.contenu|nl2br}</td>
						<td>{$ligne.temps|date_format:"%d/%m/%y %H:%M:%S"}</td>
						<td>{$ligne.pseudo}</td>
					</tr>
{/foreach}
				</tbody>
			</table>
{include file='game/footer.tpl'}