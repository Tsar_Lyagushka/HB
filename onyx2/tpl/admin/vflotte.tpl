{include file='game/header.tpl'}
			<h2>Vérification des flottes</h2>
			<table style="text-align: center; margin: auto;">
				<thead>
					<tr>
						<th>Utilisateurs</th>
						<th>Départ<br />Arrivé<br />Retour</th>
						<th>Durées</th>
						<th>Vaisseaux<br />Vitesse</th>
						<th>Contenu</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$flottes item=flotte}
					<tr{if $flotte.6} style="background-color: #{$ip.6};"{/if}>
						<td>{$flotte.0}<br />{$flotte.2}</td>
						<td>{$flotte.3} {$flotte.4}<br />{$flotte.5} {$flotte.6}</td>
						<td>{$flotte.7|date_format:"%d/%m/%y %H:%M:%S"}</td>
						<td>{$flotte.8}<br />{$flotte.9}</td>
						<td>Métal : {$flotte.10}<br />Cristal : {$flotte.11}<br />Hydrogène : {$flotte.12}</td>
					</tr>
{/foreach}
				</tbody>
			</table
{include file='game/footer.tpl'}