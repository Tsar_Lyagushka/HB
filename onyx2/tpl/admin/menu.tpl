<ul>
	<li><a href="{$menu.accueil}"{if $pagea == "accueil"} class="hilight"{/if}>Accueil</a></li>
	<li><a href="{$menu.courrier}"{if $pagea == "mail"} class="hilight"{/if}>Courrier</a></li>
	<li><a href="{$menu.webmail}" class="external">Webmail</a></li>
</ul>
<ul>
	<li><a href="{$menu.vip}"{if $pagea == "vip"} class="hilight"{/if}>Vérifier IP</a></li>
	<li><a href="{$menu.vflottes}"{if $pagea == "vflotte"} class="hilight"{/if}>Vérifier flottes</a></li>
	<li><a href="{$menu.vplanetes}"{if $pagea == "print_choixP"} class="hilight"{/if}>Vérifier planètes</a></li>
	<li><a href="{$menu.vjoueurs}"{if $pagea == "vjoueurs"} class="hilight"{/if}>Vérifier joueurs</a></li>
	<li><a href="{$menu.vrapports}"{if $pagea == "vrapports"} class="hilight"{/if}>Afficher rapports</a></li>
</ul>
<ul>
	<li><a href="?p=snalliances"{if $pagea == "snalliances"} class="hilight"{/if}>Voir les futurs alliances</a></li>
	<li><a href="{$menu.valliances}"{if $pagea == "valliances"} class="hilight"{/if}>Vérifier alliances</a></li>
</ul>
<ul>
	<li><a href="{$menu.sjoueurs}"{if $pagea == "sanctionU_choix"} class="hilight"{/if}>Sanctions joueur</a></li>
	<li><a href="{$menu.cjoueurs}"{if $pagea == "controle"} class="hilight"{/if}>Prendre le contrôle</a></li>
	<li><a href="{$menu.djoueurs}"{if $pagea == "sjoueurs"} class="hilight"{/if}>Supprimer joueurs</a></li>
	<li><a href="{$menu.inscription}"{if $pagea == "inscription"} class="hilight"{/if}>Créer lien d'inscription</a></li>
</ul>
<ul>
	<li><a href="{$menu.bandeau}"{if $pagea == "bandeau"} class="hilight"{/if}>Bandeau d'informations</a></li>
	<li><a href="{$menu.demarrage}"{if $pagea == "demarrage"} class="hilight"{/if}>Message démarrage</a></li>
	<li><a href="{$menu.versions}"{if $pagea == "version"} class="hilight"{/if}>Versions</a></li>
</ul>
<ul>
	<li><a href="./arbre.php">Arbre des technologies</a></li>
	<li><a href="./integrite.php">Vérifier l'intégrité</a></li>
	<li><a href="./lectlog.php">Lecteur de logs</a></li>
</ul>