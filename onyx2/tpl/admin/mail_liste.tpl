{include file='game/header.tpl'}
			<table style="width: 80%;">
				<thead>
					<tr>
						<th>Action</th>
						<th>Statut</th>
						<th>Emetteur</th>
						<th>Sujet</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$mails item=ligne}
					<tr{if $ligne.statut >= 1} style="background: #{if $ligne.statut == 1}770000{elseif $ligne.statut == 2}007700{elseif $ligne.statut == 3}000077{elseif $ligne.statut == 4}770077{elseif $ligne.statut == 5}007777{elseif $ligne.statut == 6}777700{/if};"{/if}>
						<td><a href="?p={$linkpage}&amp;w={$ligne.id}">STAT</a><br /><a href="?p={$linkpage}&amp;v={$ligne.id}">LIRE</a><br /><a href="?p={$linkpage}&amp;d={$ligne.id}" onclick="return confirm('Supprimer ce message ?');">SUPPR</a></td>
						<td>{$ligne.statut}</td>
						<td>{$ligne.pseudo}</td>
						<td>{$ligne.titre|escape}<br />{$ligne.contenu|escape|truncate:50}</td>
						<td>{$ligne.time|date_format:"%d/%m/%y %H:%M:%S"}</td>
					</tr>
{/foreach}
				</tbody>
			</table>
{include file='game/footer.tpl'}