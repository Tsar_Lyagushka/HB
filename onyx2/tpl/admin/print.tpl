{include file='game/header.tpl'}
			{if $linkpage == "vplanetes"}<a href="?p={$linkpage}&amp;id={$idPlan}&amp;key=hash_planete">Recalculer le hash (après déplacement d'une planète)</a>{/if}
			<table>
{foreach from=$tableau item=ligne key=key}
				<tr>
					<td><a href="?p={$linkpage}&amp;id={$idPlan}&amp;key={$key}">{$key}</a></td>
					<td>{if $ligne > 0 && (preg_match("#^time#", $key) || $key == "last_visite" || $key == "politique_lastchange")}{$ligne|date_format:"timestamp : %d/%m/%y %H:%M:%S"}{else}{$ligne|truncate:82}{/if}</td>
				</tr>
{/foreach}
			</table>
{include file='game/footer.tpl'}