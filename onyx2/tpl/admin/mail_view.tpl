{include file='game/header.tpl'}
			<table style="width: 90%;">
				<thead{if $req.statut >= 1} style="background: #{if $req.statut == 1}770000{elseif $req.statut == 2}007700{elseif $req.statut == 3}000077{elseif $req.statut == 4}770077{elseif $req.statut == 5}007777{elseif $req.statut == 6}777700{/if};"{/if}>
					<tr>
						<th colspan="2">Emetteur</th>
						<td colspan="3">{$req.pseudo}</td>
					</tr>
					<tr>
						<th colspan="2">Statut</th>
						<td colspan="3">{$req.statut}</td>
					</tr>
					<tr>
						<th colspan="2">Date</th>
						<td colspan="3">{$req.time|date_format:"%d/%m/%y %H:%M:%S"}</td>
					</tr>
					<tr>
						<th colspan="2">Objet</th>
						<td colspan="3">{$req.titre|escape}</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">{$req.contenu|escape|nl2br}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td><a href="./{$premiere_page}?p=envoyer&amp;d={$req.pseudo}&amp;o=Re : {$req.titre|escape}" onclick="window.open(this.href); return false;">Répondre</a></td>
						<td><a href="?p=courrier&amp;d={$req.id}">Supprimer</a></td>
						<td><a href="?p=courrier&amp;x={$req.id}">Statut</a></td>
						<td><a href="?p=courrier">Retour</a></td>
						<td><a href="?p=courrier&amp;v={$req.id-1}">&lt;&lt;&lt;</a> - <a href="?p=courrier&amp;v={$req.id+1}">&gt;&gt;&gt;</a></td>
					</tr>
				</tfoot>
			</table>
{include file='game/footer.tpl'}