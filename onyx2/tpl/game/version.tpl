{include file='game/header.tpl'}
			<h2>Historique des mises à jour</h2>
			<table><thead><tr><th>Versions</th><th>Détails</th></tr></thead><tbody>
{foreach from=$versions item=version}
				<tr><td><strong>{$version.numero}</strong><br />{$version.date}</td><td>{$version.description|nl2br}</td></tr>
{/foreach}
			</tbody></table>
{include file='game/footer.tpl'}