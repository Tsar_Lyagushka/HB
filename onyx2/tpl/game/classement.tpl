{include file='game/header.tpl'}
    <h2>Classement</h2>

Race : <a href="{$menu.classement}">Global</a> - <a href="{$menu.classement}&amp;r=humain">Humains</a> - <a href="{$menu.classement}&amp;r=covenant">Covenants</a>

    <table>
      <tr>
        <th>Rang</th>
        <th>Pseudo</th>
        <th>Race</th>
        <th>Points</th>
      </tr>
{foreach from=$joueurs item=joueur}
      <tr {if $joueur.id == $user}style="font-weight:bold;"{/if}>
        <th>#{$joueur.place_points}</th>
        <td>{$joueur.pseudo}</td>
        <td>{$joueur.race}</td>
        <td>{$joueur.points}</td>
      </tr>
{/foreach}
    </table>
{include file='game/footer.tpl'}
