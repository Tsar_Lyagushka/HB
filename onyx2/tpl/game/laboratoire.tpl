{include file='game/header.tpl'}
	<ul class="onglets">
		<li{if $onglet == 0} class="hilight"{/if}><a href="{$menu.laboratoire}">{$LANG[$race].technologies.branches.0|ucfirst}</a></li>
		<li{if $onglet == 1 || $onglet == 2} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=1">{$LANG[$race].technologies.branches.1|ucfirst}</a></li>
		<li{if $onglet == 3} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=3">{$LANG[$race].technologies.branches.3|ucfirst}</a></li>
		<li{if $onglet == 4} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=4">{$LANG[$race].technologies.branches.4|ucfirst}</a></li>
		<li{if $onglet == 5 || $onglet == 6} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=5">{$LANG[$race].technologies.branches.5|ucfirst}</a></li>
		<li{if $onglet == 7} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=7">{$LANG[$race].technologies.branches.7|ucfirst}</a></li>
		<li{if $onglet == 8} class="hilight"{/if}><a href="{$menu.laboratoire}&amp;n=8">{$LANG[$race].technologies.branches.8|ucfirst}</a></li>
	</ul>
	<h2>File d'attente</h2>
	<div class="file">
		<ul id="file">
{capture name='expFile'}
	{foreach from=$files item=file key=keyF}
		{foreach from=$file item=element key=keyE}
			<li>{$LANG[$race].technologies.noms_sing[$element.0][$element.1]|ucfirst} - <span{if $element.3} class="countdown"{/if}>{$element.2|countdown}</span> - <a href="{$menu.laboratoire}&amp;n={$onglet}&amp;a={$keyF}&amp;b={$keyE}">Annuler</a></li>
		{/foreach}
	{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucune recherches dans la file d'attente</li>
{/if}
		</ul>
	</div>
	<h2>{$LANG[$race].batiments.noms_sing.6|ucfirst}</h2>
{if $arbre}
	{$arbre}
{else}<div class="error" style="color: red;">Aucune technologie à développer actuellement</div>{/if}
{include file='game/footer.tpl'}
