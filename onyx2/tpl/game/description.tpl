{include file='game/header.tpl'}
		<h2>Détails {$type}</h2>
			<dl id="description">
				{if $type == 'technologies'}
				<dt>{$LANG[$raceAff][$type].noms_sing.$branche.$id|ucfirst}</dt>
				<dd><img src="{$url_images}images/{$type}/{$image}" alt="{$type|ucfirst}" /><p>{$LANG[$raceAff][$type].descriptions.$branche.$id}</p>
				{else}
				<dt>{$LANG[$raceAff][$type].noms_sing[$id]|ucfirst}</dt>
				<dd><img src="{$url_images}images/{$type}/{$image}" alt="{$type|ucfirst}" /><p>{$LANG[$raceAff][$type].descriptions.$id}</p>
				{/if}
				<div><h5>Actuellement : </h5>{if $type == 'caserne'}{$planete->casernes.$id} unité(s){elseif $type == 'terrestre'}{$planete->terrestres.$id} unité(s){elseif $type == 'vaisseaux'}{$planete->vaisseaux[$id]} vaisseau(x){elseif $type == 'technologies'}niveau {$niveau} sur {$niveau_max}{else}niveau {$planete->batiments[$id]}{/if}</div>
				{if $type == 'technologies'}
					{if $ressourcesNext.0}<div><h5>Coût {$LANG[$raceAff].ressources.noms.credits|ucfirst} : </h5>{$ressourcesNext.0|separerNombres}</div>{/if}
					{if $ressourcesNext.1}<div><h5>Temps de recherche : </h5>{$ressourcesNext.1|temps}</div>{/if}
				{else}
					{if $ressourcesNext.0}<div><h5>Coût {$LANG[$raceAff].ressources.noms.metal|ucfirst} : </h5>{$ressourcesNext.0|separerNombres}</div>{/if}
					{if $ressourcesNext.1}<div><h5>Coût {$LANG[$raceAff].ressources.noms.cristal|ucfirst} : </h5>{$ressourcesNext.1|separerNombres}</div>{/if}
					{if $ressourcesNext.2}<div><h5>Coût {$LANG[$raceAff].ressources.noms.hydrogene|ucfirst} : </h5>{$ressourcesNext.2|separerNombres}</div>{/if}
					<div><h5>Temps de construction : </h5>{$ressourcesNext.3|temps}</div>
				{/if}
{if $caract}
				<div><h5>Attaque : </h5>{$caract.0|separerNombres}</div>
				<div><h5>Bouclier : </h5>{$caract.1|separerNombres}</div>
				<div><h5>Point de vie : </h5>{$caract.2|separerNombres}</div>
				<div><h5>Capacité de transpost : </h5>{$caract.3|separerNombres}</div>
{/if}
				<div><h5>Nécessite : </h5>{$etat}</div>
{if $tableau_prod}
				<br /><br /><table style="margin: auto; width: 100%;">
					<thead>
						<tr>
							<th colspan="5">Production et consomation théoriques</th>
						</tr>
						<tr>
							<th>Niveau</th>
							<th>Production</th>
							<th>Consomation</th>
							<th>Différence prod.</th>
							<th>Différence conso.</th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$tableau_prod item=ligne}
						{if $ligne.3 == 0 && $ligne.4 == 0}<tr style="color: #F0F0F0;">{else}<tr>{/if}
							<td style="font-weight: bold;">{$ligne.0}</td>
							<td>{$ligne.1|separerNombres}</td>
							<td>{$ligne.2|separerNombres}</td>
							<td>{if $ligne.3 > 0}+{/if}{$ligne.3|separerNombres}</td>
							<td>{if $ligne.4 > 0}+{/if}{$ligne.4|separerNombres}</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
{/if}
				</dd>
			</dl>
{include file='game/footer.tpl'}
