{include file='game/header.tpl'}
{include file='game/diplomatie/common.tpl'}
			<h2>Déclarations de guerre</h2>
{if $guerres}
			<ul>
	{foreach from=$guerres item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> depuis le {$pacte.time_creation|date_format:"%d/%m/%y"}{if ($planete->id == $pacte.id_alliance1 && $pacte.accepte != 2) || ($planete->id == $pacte.id_alliance2 && $pacte.accepte == 2)}{if $pacte.accepte == 0} - Une demande de cesser le feu est en cours{else}{if $planete->permissions_alliance & 8} - <em><a href="{$menu.diplomatie}&amp;o=new&amp;c={$pacte.id}">Demander un cesser le feu</a></em>{else} - Nous pouvons demander le cesser le feu{/if}{/if}{elseif $pacte.accepte == 0}{if $planete->permissions_alliance & 8} - <em><a href="{$menu.diplomatie}&amp;a={$pacte.id}">Accepter le cesser le feu</a></em> - <em><a href="{$menu.diplomatie}&amp;r={$pacte.id}">Refuser le cesser le feu</a></em>{else} - Nos adversaires nous propose un cesser le feu{/if}{else} - Nous ne pouvons pas demander de cesser le feu.{/if}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucune guerre n'est actuellement déclarée</h3>
{/if}
			<br />
			<h2>Pactes de non agression</h2>
{if $pnas}
			<ul>
	{foreach from=$pnas item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> depuis le {$pacte.time_creation|date_format:"%d/%m/%y"}{if $planete->permissions_alliance & 8} - <em><a href="{$menu.diplomatie}&amp;s={$pacte.id}">Abroger le pacte</a></em>{/if}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'est actuellement en vigueur</h3>
{/if}
			<br />
			<h2>Pactes commerciaux</h2>
{if $pcs}
			<ul>
	{foreach from=$pcs item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> depuis le {$pacte.time_creation|date_format:"%d/%m/%y"}{if $planete->permissions_alliance & 8} - <em><a href="{$menu.diplomatie}&amp;s={$pacte.id}">Abroger le pacte</a></em>{/if}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'est actuellement en vigueur</h3>
{/if}
			<br />
			<h2>Pactes militaires</h2>
{if $pms}
			<ul>
	{foreach from=$pms item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> depuis le {$pacte.time_creation|date_format:"%d/%m/%y"}{if $planete->permissions_alliance & 8} - <em><a href="{$menu.diplomatie}&amp;s={$pacte.id}">Abroger le pacte</a></em>{/if}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'est actuellement en vigueur</h3>
{/if}
{include file='game/footer.tpl'}