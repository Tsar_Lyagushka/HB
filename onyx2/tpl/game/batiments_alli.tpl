{include file='game/header.tpl'}
		<h2>File d'attente</h2>
		<ul id="file">
{capture name='expFile'}
	{foreach from=$files item=file key=keyF}
		{foreach from=$file item=element key=keyE}
			<li>{$LANG[$race].alli_batiments.noms_sing[$element.0]|ucfirst}{if $element.1} (démolition){/if} - <span{if $element.3} class="countdown"{/if}>{$element.2|countdown}</span> - <a href="{$menu.batiments}&amp;a={$keyF}&amp;b={$keyE}">Annuler</a></li>
		{/foreach}
	{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucun b&acirc;timent dans la file d'attente</li>
{/if}
		</ul>
		<h3><a href="{$menu.arbre}&amp;q=alli_batiments">Arbre des technologies</a></h3><br />
		<h2>B&acirc;timents</h2>
		<div id="constructions">
{foreach from=$batiments item=batiment}
			<dl>
				<dt>{$LANG[$race].alli_batiments.noms_sing[$batiment.id]|ucfirst}{if $batiment.niveau > 0} (Niveau {$batiment.niveau}){/if}</dt>
				<dd class="description"><a href="?p=description&amp;a={$batiment.id}#body"><img src="{$url_images}images/alli_batiments/{$batiment.id}.jpg" alt="{$LANG[$race].alli_batiments.noms_sing[$batiment.id]}" /></a><p>{$LANG[$race].alli_batiments.descriptions[$batiment.id]}</p></dd>
				<dd>
					{if $batiment.niveau > 0}Niveau actuel : <em>{$batiment.niveau}</em><br /><br />{/if}
					{if $batiment.nec_metal > 0}Coût {$LANG[$race].ressources.noms.metal} : <em{if $batiment.nec_metal > $planete->metal} class="lack"{/if}>{$batiment.nec_metal|separerNombres}</em><br />{/if}
					{if $batiment.nec_cristal > 0}Coût {$LANG[$race].ressources.noms.cristal} : <em{if $batiment.nec_cristal > $planete->cristal} class="lack"{/if}>{$batiment.nec_cristal|separerNombres}</em><br />{/if}
					{if $batiment.nec_hydrogene > 0}Coût {$LANG[$race].ressources.noms.hydrogene} : <em{if $batiment.nec_hydrogene > $planete->hydrogene} class="lack"{/if}>{$batiment.nec_hydrogene|separerNombres}</em><br />{/if}
					{if $batiment.nec_credits > 0}Coût {$LANG[$race].ressources.noms.credits} : <em{if $batiment.nec_credits > $planete->credits_alliance} class="lack"{/if}>{$batiment.nec_credits|separerNombres}</em><br />{/if}
					Temps de construction : <em>{$batiment.temps}</em><br />
					<br />
					{if $batiment.nec_credits > $planete->credits_alliance || $batiment.nec_hydrogene > $planete->hydrogene || $batiment.nec_cristal > $planete->cristal || $batiment.nec_metal > $planete->metal}<span class="lack" title="Rassemblez les ressources manquantes avant de lancer la construction de ce batiment">Ressources insuffisantes</span>
					{else}<a href="{$menu.batiments}&amp;c={$batiment.id}">Construire</a>{/if}
					{if $batiment.enfile !== false}<a href="{$menu.batiments}&amp;a={$batiment.enfile.0}&amp;b={$batiment.enfile.1}">Arrêter</a>{/if}
					{if $batiment.niveau > 0 && $batiment.id != 0} <a href="{$menu.batiments}&amp;d={$batiment.id}">Démolir un niveau</a>{/if}
				</dd>
			</dl>
{/foreach}
		</div>
{include file='game/footer.tpl'}