{include file='game/header.tpl'}
			<form action="{$menu.flotte}" method="post">
			<h2>Paramètres généraux</h2>
				<fieldset class="options">
					<label for="nom">Nom de la flotte :</label><input class="text" type="text" id="nom" name="nomflotte" maxlength="24" /><br />
					<label for="fav_dest">Destination rapide :</label><select name="destin" id="fav_dest"><option value="0">--</option><optgroup label="Colonies">{html_options options=$favorisColonies}</optgroup><optgroup label="Favoris">{html_options options=$favoris}</optgroup><option value="edit">Modifier la liste</option></select><br />
					<label for="amas">Destination :</label><span id="destination"><input class="dest" type="text" id="amas" name="amas" maxlength="2" />:<input class="dest" type="text" id="ss" name="ss" maxlength="2" />:<input class="dest" type="text" id="plan" name="pos" maxlength="2" /></span><br />
					<label for="vitesse">Vitesse :</label><select name="vitesse" id="vitesse"><option value="100">100%</option><option value="75">75%</option><option value="50">50%</option><option value="25">25%</option></select><br />
					<br /><div id="aide1">Pour vous repérer plus facilement entre vos différentes flottes, donnez-lui un nom.</div>
				</fieldset>
			<h2>Mission et contenu</h2>
				<fieldset class="options">
					<label for="mission">Mission :</label>
						<select name="mission" id="mission" onchange="tempsFlotte();">
							{html_options options=$missions}
						</select><br />
					<span id="tactique"></span>
					<label for="metal">Embarquer {$LANG[$race].ressources.noms.metal} :</label><input class="text" type="text" id="metal" name="metal" /><br />
					<label for="cristal">Embarquer {$LANG[$race].ressources.noms.cristal} :</label><input class="text" type="text" id="cristal" name="cristal" /><br />
					<label for="hydrogene">Embarquer {$LANG[$race].ressources.noms.hydrogene} :</label><input class="text" type="text" id="hydrogene" name="hydrogene" /><br />
					<label>Places restantes dans les cales :</label><span id="placesRest"></span><br />
					<br /><div id="aide2"></div>
				</fieldset>
			<h2>Envoyer</h2>
				<noscript><span style="color: #FF0000; font-weight: bolder;">Le JavaScript est nécessaire pour envoyer une flotte.</span><br /></noscript>
				<input type="submit" value="GO" class="submit" /><input type="hidden" name="cds" value="123456" /><br />
				Temps de déplacement : <span id="temps">-</span><br />
				Consomation : <span id="conso">-</span><br />
				<span id="deblok"><b>Compl&egrave;tez les champs ci-dessus</b></span>
				<br /><span id="vp"></span>
			</form>
{include file='game/footer.tpl'}