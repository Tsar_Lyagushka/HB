{include file='game/header.tpl'}
		<ul class="onglets">
			<li{if $onglet == 0} class="hilight"{/if}><a href="{$menu.batiments}">Tous</a></li>
			<li{if $onglet&1} class="hilight"{/if}><a href="{$menu.batiments}&amp;n=1">Mines/centrales</a></li>
			<li{if $onglet&2} class="hilight"{/if}><a href="{$menu.batiments}&amp;n=2">Civils</a></li>
			<li{if $onglet&4} class="hilight"{/if}><a href="{$menu.batiments}&amp;n=4">Militaires</a></li>
		</ul>

		<h2>File d'attente</h2>
		<ul id="file">
{capture name='expFile'}
	{foreach from=$files item=file key=keyF}
		{foreach from=$file item=element key=keyE}
			<li>{$LANG[$race].batiments.noms_sing[$element.0]|ucfirst}{if $element.1} (démolition){/if} - <span{if $element.3} class="countdown"{/if}>{$element.2|countdown}</span> - <a href="{$menu.batiments}&amp;a={$keyF}&amp;b={$keyE}">Annuler</a></li>
		{/foreach}
	{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucun b&acirc;timent dans la file d'attente</li>
{/if}
		</ul>
		<h3><a href="{$menu.arbre}&amp;q=batiments">Arbre des technologies</a></h3><br />
		<h2>B&acirc;timents</h2>
		<div id="constructions">
{foreach from=$batiments item=batiment}
			<dl>
				<dt>{$LANG[$race].batiments.noms_sing[$batiment.id]|ucfirst}{if $batiment.niveau > 0} (Niveau {$batiment.niveau}){/if}</dt>
				<dd class="description"><a href="?p=description&amp;b={$batiment.id}#body"><img src="{$url_images}images/batiments/{$batiment.image}" alt="{$LANG[$race].batiments.noms_sing[$batiment.id]}" /></a><p>{$LANG[$race].batiments.descriptions[$batiment.id]}</p></dd>
				<dd>
					{if $batiment.niveau > 0}Niveau actuel : <em>{$batiment.niveau}</em><br /><br />{/if}
					{if $batiment.nec_metal > 0}Coût {$LANG[$race].ressources.noms.metal} : <em{if $batiment.nec_metal > $planete->metal} class="lack"{/if}>{$batiment.nec_metal|separerNombres}</em><br />{/if}
					{if $batiment.nec_cristal > 0}Coût {$LANG[$race].ressources.noms.cristal} : <em{if $batiment.nec_cristal > $planete->cristal} class="lack"{/if}>{$batiment.nec_cristal|separerNombres}</em><br />{/if}
					{if $batiment.nec_hydrogene > 0}Coût {$LANG[$race].ressources.noms.hydrogene} : <em{if $batiment.nec_hydrogene > $planete->hydrogene} class="lack"{/if}>{$batiment.nec_hydrogene|separerNombres}</em><br />{/if}
					Temps de construction : <em>{$batiment.temps}</em><br />
					<br />
					{if $batiment.nec_hydrogene > $planete->hydrogene || $batiment.nec_cristal > $planete->cristal || $batiment.nec_metal > $planete->metal}<span class="lack" title="Rassemblez les ressources manquantes avant de lancer la construction de ce batiment">Ressources insuffisantes</span>
					{else}{if $planete->casesRest >= 1}<a href="{$menu.batiments}&amp;c={$batiment.id}">Construire</a>{/if}{/if}
					{if $batiment.enfile !== false}<a href="{$menu.batiments}&amp;a={$batiment.enfile.0}&amp;b={$batiment.enfile.1}">Arrêter</a>{/if}
					{if $batiment.niveau > 0} <a href="{$menu.batiments}&amp;d={$batiment.id}">Démolir un niveau</a>{/if}
				</dd>
			</dl>
{/foreach}
		</div>
{include file='game/footer.tpl'}