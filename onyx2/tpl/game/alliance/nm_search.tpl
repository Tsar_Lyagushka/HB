{include file='game/header.tpl'}
			<table>
				<thead>
					<tr>
						<th>[TAG] Alliance</th>
						<th>Race</th>
						<th>Etat inscriptions </th>
					</tr>
				</thead>
				<tbody>
{foreach from=$recherches item=recherche}
					<tr>
					{if $recherche.etat_inscription == 2}
						<td><a href="{$menu.alliance}&amp;signer={$recherche.id}">[{$recherche.tag|upper|escape}] {$recherche.nom_alliance|escape}</a></td>
						<td>En fondation</td>
						<td>En fondation</td>
					{else}
						<td><a href="{$menu.alliance}&amp;v={$recherche.id}">[{$recherche.tag|upper|escape}] {$recherche.nom_alliance|escape}</a></td>
						<td><img src="{$url_images}images/carte/{$recherche.race}.jpg" width="45" alt="{$recherche.race}" /></td>
						<td>{if $recherche.etat_inscription}Ouvertes{else}Fermées{/if}</td>
					{/if}
					</tr>
{/foreach}
				</tbody>
			</table>
{include file='game/footer.tpl'}