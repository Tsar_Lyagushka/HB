{include file='game/header.tpl'}
			<h2>Chat</h2>
<applet name="coolsmile" code="EIRC.class" style="height: 450px; width: 98%;">
	<param name="archive" value="java/EIRC.jar,java/EIRC-cfg.jar">
	<param name="cabbase" value="java/EIRC.cab,java/EIRC-cfg.cab">
	<param name="server" value="{$planete->url_chat}">
	<param name="port" value="{$planete->details.port_chat}">
	<param name="irc_pass" value="{$planete->details.pass_chat}">

	<param name="font_name" value="Helvetica">
	<param name="font_size" value="11">
	<param name="language" value="">
	<param name="mainbg" value="{if $race == "humain"}#2E3122{else}#2E293D{/if}">
	<param name="mainfg" value="{if $race == "humain"}#999966{else}#6993A7{/if}">
	<param name="textbg" value="#FFFFFF">
	<param name="textfg" value="#000000">
	<param name="selbg" value="#F0F0FF">
	<param name="selfg" value="#000000">

	<param name="join" value="{if !ereg("#", $planete->details.chan_chat)}#{/if}{$planete->details.chan_chat}">
	<param name="username" value="Appletweb">
	<param name="realname" value="">
	<param name="nickname" value="{$planete->pseudo}">
	<param name="nicksrv_pass" value="">
	<param name="login" value="1">
	<param name="asl" value="1">
	<param name="spawn_frame" value="0">
	<param name="gui_nick" value="1">

	<param name="gui_away" value="1">
	<param name="gui_chanlist" value="1">
	<param name="gui_userlist" value="1">
	<param name="gui_options" value="1">
	<param name="gui_help" value="1">
	<param name="gui_connect" value="1">
	<param name="width" value="700">
	<param name="height" value="500">
	<param name="write_color" value="12">

	<param name="debug_traffic" value="0">
	<param name="boxmessage" value="Patientez quelques instants svp ...">
	<param name="boxbgcolor" value="blue">
	<param name="boxfgcolor" value="black">
	<param name="progressbar" value="true">
	<param name="progresscolor" value="red">
</applet>
	<br /><br />Vous pouvez aussi utiliser votre propre client IRC : <a href="irc:{$planete->url_chat}:{$planete->details.port_chat}{if !ereg("#", $planete->details.chan_chat)}#{/if}{$planete->details.chan_chat}">irc:{$planete->url_chat}:{$planete->details.port_chat}{if !ereg("#", $planete->details.chan_chat)}#{/if}{$planete->details.chan_chat}</a><br />
{include file='game/footer.tpl'}