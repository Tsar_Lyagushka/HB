{include file='game/header.tpl'}
			<h2>Ratifier l'alliance : {$alliance.nom_alliance|capitalize|escape}</h2>
			<form action="{$menu.alliance}&amp;signer={$alliance.id}" method="post">
				<div><strong>Vous êtes sur le point de ratifier l'alliance [{$alliance.tag|upper|escape}] {$alliance.nom_alliance|escape}.<br />Actuellement, {$alliance.nbsignatures} personnes(s) ont ratifier cette alliance.<br />{if $ratifier}<br />En ratifiant cette alliance, vous annulez votre signature pour l'alliance [{$ratifier.tag|upper|escape}] {$ratifier.nom_alliance|escape}<br />{/if}<br />Voulez-vous signer ?</strong></div>
				<fieldset class="form">
					<input type="submit" class="submit" value="Oui " name="sign" /> - <input type="submit" class="submit" value="Non " name="sign" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}