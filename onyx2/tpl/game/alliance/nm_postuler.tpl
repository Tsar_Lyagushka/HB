{include file='game/header.tpl'}
			<h2>Postuler pour l'alliance : {$alliance.nom_alliance|capitalize|escape}</h2>
			<form action="{$menu.alliance}&amp;postuler={$alliance.id}" method="post">
				<fieldset class="form">
					<p>{$alliance.message_inscription|escape|bbcode|nl2br}</p>
					<label for="motivation">Message de motivation : <textarea name="motivation" id="motivation"></textarea></label><br />
					<input type="submit" class="submit" value="Ok" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}