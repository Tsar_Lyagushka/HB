{include file='game/header.tpl'}
			<h2>Cr&eacute;ation d'une alliance</h2>
			<form method="post" action="{$menu.alliance}&amp;q=fonder">
				<fieldset class="form">
					<p>Cr&eacute;er une alliance ne peut pas se faire seul. Une fois que vous aurez lanc&eacute; la proc&eacute;dure, un lien vous sera donn&eacute;. Pour finir la cr&eacute;ation, donnez ce lien &agrave; quatre personnes qui sont sans alliance afin qu'elles ratifient l'alliance.</p>
					<label for="nom">Nom : <input type="text" class="text" name="nom" id="nom" /></label><br />
					<label for="tag">Tag : <input type="text" class="text" name="tag" id="tag" maxlength="5" /></label><br />
					<input type="submit" class="submit" value="Cr&eacute;er  " />
				</fieldset>
			</form>
{include file='game/footer.tpl'}