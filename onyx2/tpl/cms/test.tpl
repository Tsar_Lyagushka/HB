{include file='cms/header.tpl'}
		<div id="connexion" class="block">
			<div class="header">Connexion</div>
			<div class="corps"><form action="#" method="post"><fieldset><span><input type="text" class="text" name="login" maxlength="15" tabindex="1" /><input type="password" class="password" name="password" maxlength="30" tabindex="2" /><input type="submit" class="submit" value="GO" tabindex="3" /></span><span><input type="checkbox" name="cookie" id="cookie" /><label for="cookie">Retenir mes informations</label> <a href="#" tabindex="4">Mot de passe oublié</a></span></fieldset></form></div>
		</div>

		<div id="inscription" class="block"><span><a href="{$link.inscription}">INSCRIVEZ VOUS</a>  Rejoignez l'alliance ou engagez-vous</span> <p><strong>DEFENDREZ VOUS L'HUMANITE OU LA DETRUIREZ VOUS?</strong><br /><br />Repoussez l'ennemi et partez a la conquète des mondes-anneaux dans des univers vastes et riches basés sur l'univers du jeu vidéo Halo<br /><br /><em>Une simple inscription gratuite et un navigateur internet sont demandés pour pouvoir participer a des batailles épiques!</em></p> <a href="{$link.inscription}" class="link"><span>Cliquez ici pour vous inscrire</span></a> <a href="{$link.inscription}" class="more">En savoir plus</a></div>

		<div id="news" class="block">
			<h2>Actualités</h2>
			<h3><span>15/06/07</span></h3>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<h3><span>14/06/07</span></h3>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<div>
				<img src="images/accueil/news_img.jpg" alt="" width="50" height="50" />
				<p><strong>Ghosts of Onyx</strong> dans : <ins>Halo</ins> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
			</div>
			<div class="bottom"><a href="#">Plus de news</a> <span>1,<a href="#">2</a>,<a href="#">3</a>,<a href="#">4</a>  <a href="#">&#60;&#60;</a> <a href="#">&#60;</a> <a href="#">&#62;</a> <a href="#">&#62;&#62;</a></span></div>
		</div>

		<div id="annonce" class="block">
			<div><img src="images/accueil/annonce.jpg" alt="" width="452" height="174" /><a href="#" class="link"><span>NAVIGATION &#62;&#62;</span></a></div>
			<p><strong><span>Ouverture de Halo-Battle</span></strong> <cite><em>" Votre Excellence...<br />- J'ai demandé à ne pas être dérangé. J'espère que vous avez une bonne raison de venir me troubler dans mes méditations.<br />- Votre Excellence... Il est enfin là.<br />- Quoi donc ? Parle, 'Vatinree.<br />- Ce pourquoi nous attendons en ce lieu depuis des années, Excellence ! C'est un jour béni par les Dieux !<br />- Ainsi donc... Cela se peut-il ?... "</em><br /><br />Cette conversation entre ces deux Sangheili vous semble improbable ?<br />Mais de quoi donc peuvent-ils discuter ?<br />Ceux qui suivent Halo-Battle depuis longtemps auront peut-être la puce à l'oreille (ou peut-être avez-vous la puce à l'oreille tout court).<br />Oui, cette fois, c'est bel et bien terminé : Halo-Battle va enfin voir le jour, après des années de rebondissements, d'espoirs incensés et d'efforts hors du commun pour donner un souffle de vie à ce projet.<br />La chose assez paradoxale est que l'on peut voir cet heureux dénouement comme une sorte de fin en soit (pour ceux qui suivent depuis le début le projet par exemple), alors que ce n'est le début que d'une longue carrière et d'une volonté de tendre vers la perfection, en améliorant et nourrissant Halo-Battle d'idées et de suggestions toutes plus grandioses les unes que les autres.<br />Préparez-vous donc non pas à finir le combat, mais à le commencer justement ! Rendez-vous le XX/XX/XXXX.</cite> <a href="#" class="com">9 commentaires</a> <a href="#" class="plus">PLUS &#62;&#62;</a></p>
		</div>

		<div id="statistiques" class="block">
			<div class="header">Statistiques</div>
			<div class="corps"><img src="images/accueil/stat.png" alt="Statistiques" /></div>
			<div class="footer"><a href="#">Plus de statistiques</a></div>
		</div>

		<div id="container" class="block">
			<div id="sondage">
				<h2>Sondage</h2>
				<div class="header"><span>Semaine du xx/xx au xx/xx</span></div>
				<div class="corps"><form action="#" method="post"><fieldset>
				<label for="sondage1"><input type="radio" class="radio" name="sondage" value="1" id="sondage1" checked="checked" /> Choix 1</label>
				<label for="sondage2"><input type="radio" class="radio" name="sondage" value="2" id="sondage2" /> Choix 2</label>
				<label for="sondage3"><input type="radio" class="radio" name="sondage" value="3" id="sondage3" /> Choix 3</label>
				<label for="sondage4"><input type="radio" class="radio" name="sondage" value="4" id="sondage4" /> Choix 4</label>
				<label for="sondage5"><input type="radio" class="radio" name="sondage" value="5" id="sondage5" /> Choix 5</label>
				<input type="submit" class="submit" value="OK" />
				</fieldset></form></div>
			</div>
			<div id="dossier">
				<h2>Dossiers</h2>
				<div class="header"><span></span></div>
				<div class="corps"><img src="images/accueil/dossier.jpg" alt="" width="45" height="45" /><p><strong>DOSSIER HALO WARS</strong> <cite>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.</cite></p></div>
				<div class="footer"><a href="#">Plus de dossiers</a></div>
			</div>
		</div>
{include file='cms/footer.tpl'}